﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnPress()
    {
        anim.SetTrigger("OpenDoor");
    }
    public void closedoor()
    {
        anim.enabled = true;
    }
    public void pauseAnimationEvent()
    {
        anim.enabled = false;
    }
}
