﻿using System.Collections;
using UnityEngine;

public class RaycastAction : MonoBehaviour
{
	private GameObject playerCamera;
	public GameObject signHit;
	public GameObject doorHit;
	public GameObject knobHit;
	public GameObject sign_keepout;
	public GameObject Whiteness;
	public GameObject ReachableFloor;
	private GameObject Light_behindDoor;

	public Door doorscript;
    public Knob knobscript;
    float knobTurned;
    public AudioClip doorSound;
    public AudioClip knobSound;
    public AudioClip openDoorSound;
    public AudioClip closeDoorSound;
    public AudioClip slamDoorSound;
	public AudioClip kidAnswer;
	public AudioClip kidAnswerB;
	private AudioSource Audio;
	public bool dooropen;
	public bool signGrabbed;

 	bool alreadyKnocked;
	bool thrown;
	bool gettingSign;
	bool clicked;

	Vector3 targetPosition;
	Vector3 finalPosition;
	float counter = 0f;


	private void OnEnable()
    {
        SceneController.actionCallback += Click;
    }
    private void OnDisable()
    {
        SceneController.actionCallback -= Click;
    }

    void Click()
    {
        clicked = true;

    }

    // Start is called before the first frame update
    void Start()
    {
        Audio = doorHit.GetComponent<AudioSource>();
		dooropen = false;
		signGrabbed = false;
		Audio.PlayOneShot(slamDoorSound, 0.7f);
        playerCamera = Camera.main.gameObject;
		Whiteness.SetActive(false);
		gettingSign = false;
		sign_keepout.GetComponent<Rigidbody>().useGravity = false;
		sign_keepout.GetComponent<Rigidbody>().isKinematic = true;
		alreadyKnocked = false;
		thrown = false;
		finalPosition.y = ReachableFloor.transform.position.y;
		//Light_behindDoor = GameObject.Find("Light_behindDoor");
		//if(Light_behindDoor) Light_behindDoor.SetActive(false);

	}




	// Update is called once per frame
	void Update()
    {

        //wait for the scene to completely fade in, otherwise some items may stay invisible when altering materials
        if (SceneController.IsPreparing)
            return;

        RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);


        if (Physics.Raycast(ray, out hit))
        {

			if (hit.collider.CompareTag("sign_keepout"))
			{
				signHit.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);

				signHit = hit.collider.gameObject;

		
				if (clicked && signGrabbed == false)
				{

					targetPosition.z = (playerCamera.transform.position.z * 0.4f);
					targetPosition.x = playerCamera.transform.position.x;
					targetPosition.y = sign_keepout.transform.position.y;


					StartCoroutine(GrabbingSign());
					signGrabbed = true;

					clicked = false;
					Audio.PlayOneShot(kidAnswerB, 0.9f);
				}

			}
			else
			{
				signHit.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);

			}



			
			if (hit.collider.CompareTag("Door"))
            {
				if (!signGrabbed)
				{
					doorHit.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);

					doorHit = hit.collider.gameObject;


					if (clicked && signGrabbed == false & alreadyKnocked == true)
					{
						clicked = false;
						Audio.PlayOneShot(doorSound, 0.7f);

					}

					if (clicked && signGrabbed == false & alreadyKnocked == false)
					{
						clicked = false;
						alreadyKnocked = true;
						Audio.PlayOneShot(doorSound, 0.7f);
						Audio.PlayOneShot(kidAnswer, 0.9f);
					}
				}
			}
			else
            {
                doorHit.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);

            }

            if (hit.collider.CompareTag("Knob"))
            {
                knobHit.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);

                knobHit = hit.collider.gameObject;

				if (clicked && signGrabbed == true && dooropen == false)
				{
					doorscript.OnPress();
					dooropen = true;
					clicked = false;
					Audio.PlayOneShot(openDoorSound, 0.7f);
					//if(Light_behindDoor) Light_behindDoor.SetActive(true);
					Whiteness.SetActive(true);
					Audio.PlayOneShot(knobSound, 0.7f);
					StartCoroutine(EndScene());

				}

				else if (clicked)
				{
					clicked = false;
					knobscript.OnPress();
					knobTurned = knobTurned + 1;
					Audio.PlayOneShot(knobSound, 0.7f);

				}
			}




			else
            {
                knobHit.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
                knobHit.GetComponent<MeshRenderer>().material.SetFloat("_OutlineThickness", 0.0f);
            }
      
        }
		if (gettingSign == true && counter <= 0.5f)
		{
			sign_keepout.transform.localRotation = playerCamera.transform.rotation;
			counter += Time.deltaTime;
			clicked = false;

		}
		else if (gettingSign == true && counter > 0.5f)
		{
			counter = 0f;
			gettingSign = false;



			StartCoroutine(ThrowSign());
		}
	}

	IEnumerator EndScene()
    {
        yield return new WaitForSeconds(3);
        Debug.Log("Next Scene");
        SceneController.EndScene();
    }


	public IEnumerator GrabbingSign()
	{

		Transform t = sign_keepout.GetComponent<Transform>();
		Vector3 originalPosition = t.position;


		counter = 0f;
		while (counter < 0.5f)
		{
			t.position = Vector3.Lerp(t.position, targetPosition, counter);
			counter += Time.deltaTime;
			///yield return 0;
			yield return null;

		}
		clicked = false;

		counter = 0f;
		gettingSign = true;
		yield break;
	}




	public IEnumerator ThrowSign()
	{
		sign_keepout.GetComponent<Rigidbody>().useGravity = true;
		sign_keepout.GetComponent<Rigidbody>().isKinematic = false;
		////sign_keepout.GetComponent<Collider>().enabled = false;

		 sign_keepout.transform.localRotation = Quaternion.Euler((playerCamera.transform.eulerAngles.x + 55.5f), playerCamera.transform.eulerAngles.y, playerCamera.transform.eulerAngles.z);

		finalPosition.z = (playerCamera.transform.position.z * 0.08f);
		finalPosition.x = (playerCamera.transform.position.x * 1.01f);

		Transform tt = sign_keepout.GetComponent<Transform>();
		Vector3 originalPositionB = tt.position;


		counter = 0f;
		while (counter < 0.5f)
		{
			tt.position = Vector3.Lerp(tt.position, finalPosition, counter);
			counter += Time.deltaTime;
			///yield return 0;
			yield return null;

		}

		thrown = true;

		//// Destroy(piggybank);


		/////sign_keepout.SetActive(false);



		yield break;
	}




}


