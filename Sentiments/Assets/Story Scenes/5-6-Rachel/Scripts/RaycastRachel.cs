﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RaycastRachel : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject playerCamera;

    public int speed = 2;
    public int redCol;
    public int greenCol;
    public int blueCol;
    public bool lookingAtObject = false;
    public bool flashingIn = true;
    public bool startedFlashing = false;


    // public GameObject RachelFurniture;
    public GameObject RachelWalls;
    public GameObject floor;
    public GameObject trigger;

    public GameObject drawer;


    public GameObject alarmclock;
    public GameObject piggybank;

    enum State { ALARM, DRAWER, OPEN, HOLDING, THROW, BROKEN }

    State state;

    AudioSource Alarmbeep;
    AudioSource drawerNoise;
    Animation drawerOpen;

    public bool canHold = true;
    public GameObject item;
    public GameObject tempParent;
    Piggy piggy;
    bool action = false;


    void Start()
    {
        state = State.ALARM;


        trigger.SetActive(true);
        piggybank.SetActive(false);
        alarmclock.SetActive(true);

        //Set gravity to false while holding it
        piggybank.GetComponent<Rigidbody>().useGravity = false;
        piggybank.GetComponent<Collider>().enabled = false;
        piggybank.GetComponent<Rigidbody>().isKinematic = true;

        Alarmbeep = alarmclock.GetComponent<AudioSource>();
        drawerOpen = drawer.GetComponent<Animation>();
        drawerNoise = drawer.GetComponent<AudioSource>();

        piggy = piggybank.GetComponent<Piggy>();
        playerCamera = Camera.main.gameObject;

    }

    private void OnEnable()
    {
        SceneController.actionCallback += Click;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= Click;
    }

    void Click()
    {


        if (state == State.HOLDING)
        {
            state = State.THROW;
            piggy.Throw();

        }
        else
            action = true;


    }

    void Update()
    {

        //wait for the scene to completely fade in, otherwise some items may stay invisible when altering materials
        if (SceneController.IsPreparing)
            return;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //   update the piggy to player and set broken piggy and coin positions  ////////////////////////
        if (state == State.HOLDING)
        {
            piggybank.transform.position = (playerCamera.transform.forward * 0.8f) + playerCamera.transform.position;
            piggybank.transform.localRotation = playerCamera.transform.rotation;

        }


        RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        //Debug to show the actual ray in Scene
        //Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("alarm_rachel"))
            {
                if (state == State.ALARM)
                {


                    alarmclock.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);
                    if (action)
                    {
                        action = false;
                        state = State.DRAWER;
                        Alarmbeep.Stop();
                        alarmclock.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
                        piggybank.SetActive(true);

                    }
                }
            }

            else if (hit.collider.CompareTag("drawer_rachel"))
            {

                if (state == State.DRAWER) // 2: Open the drawer
                {
                    drawer.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);
                    if (action)
                    {
                        action = false;
                        state = State.OPEN;
                        drawerNoise.Play();
                        drawerOpen.Play();
                        drawer.GetComponent<Collider>().enabled = false;
                        piggybank.GetComponent<Collider>().enabled = true;
                        drawer.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
                    }
                }

            }
            // 3: Grab the piggybank
            else if (hit.collider.CompareTag("piggy_rachel"))
            {
                if (state == State.OPEN)
                {
                    piggybank.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);
                    if (action)
                    {
                        action = false;
                        state = State.HOLDING;
                        piggybank.transform.SetParent(null, true);
                        piggybank.GetComponent<Collider>().enabled = false;
                        piggybank.GetComponent<Rigidbody>().isKinematic = true;
                        piggybank.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
                    }
                }
            }
            else //collided with something else
            {
                piggybank.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
                alarmclock.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
                drawer.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);



            }

        }
        else  //not looking at anything special
        {
            piggybank.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
            alarmclock.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
            drawer.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);

        }

    }

}



