﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piggy : MonoBehaviour
{
    public GameObject Brokenpiggy;
    public GameObject coins_container;
    AudioSource coins;
    AudioSource breaking;
    bool throwed = false;
    public bool isBroken = false;

    // Start is called before the first frame update
    void Start()
    {
        Brokenpiggy.SetActive(false);
        coins_container.SetActive(false);
        breaking = Brokenpiggy.GetComponent<AudioSource>();
        coins = coins_container.GetComponent<AudioSource>();
        throwed = false;
        isBroken = false;
    }

    public void Throw()
    {
        throwed = true;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Collider>().enabled = true;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(throwed && isBroken == false)
        {
            if (collision.gameObject.CompareTag("floor_rachel"))
            {
                Brokenpiggy.transform.position = transform.position;
                coins_container.transform.position = transform.position;

                GetComponent<MeshRenderer>().enabled = false;

                Brokenpiggy.SetActive(true);
                coins_container.SetActive(true);

                coins.Play();
                breaking.Play();

                isBroken = true;
                StartCoroutine("EndScene", 4f);
            }
        }
    }             

    IEnumerator EndScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.EndScene();

    }

}
