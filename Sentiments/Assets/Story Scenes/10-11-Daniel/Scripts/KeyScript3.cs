﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript3 : MonoBehaviour
{
    public GameObject key;
    public GameObject doorKnob;
    private GameObject playerCamera;
    bool parenting;
    public GameObject raycast;

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;
        playerCamera = Camera.main.gameObject;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
    }

    private void ActionButtonPress()
    {
        if (raycast.GetComponent<RayDanielScene>().raycollider == GetComponent<Collider>())
        {
            doorKnob.GetComponent<doorKnobScript>().hasKey = true;

            parenting = true;

            GetComponent<Collider>().enabled = false;
            transform.GetChild(0).gameObject.SetActive(false);
            Debug.Log("hi");
        }
                
        
    }
    void Update()
    {
        if (parenting)
        {
            transform.position = playerCamera.transform.forward *.5f + playerCamera.transform.position;
            transform.localRotation = playerCamera.transform.rotation;

        }
    }
}
