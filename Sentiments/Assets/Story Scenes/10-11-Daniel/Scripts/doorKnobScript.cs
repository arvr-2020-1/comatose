﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorKnobScript : MonoBehaviour
{
    public bool hasKey;
    public GameObject doorHinge;
    public GameObject key;
    public GameObject raycast;
    private int time;
    public GameObject lightOne;
    public GameObject lightTwo;

    private void OnEnable()
    {
        time = 0;
        SceneController.actionCallback += ActionButtonPress;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
    }

    private void ActionButtonPress()
    {
        if (hasKey)
        {
            if (raycast.GetComponent<RayDanielScene>().raycollider == GetComponent<Collider>())
            {
                hasKey = false;
                //key.SetActive(false);

                StartCoroutine(EndScene(1));
            }
        }
    }

    IEnumerator EndScene(float duration)
    {
        while (time <= 120)
        {
            doorHinge.transform.Rotate(0, -.875f, 0);
            time++;
            yield return new WaitForSeconds(duration / 120);
            lightOne.SetActive(true);
            lightTwo.SetActive(true);
        }
        Debug.Log("check");
        SceneController.EndScene();
    }
}
