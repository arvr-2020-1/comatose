﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneScript : MonoBehaviour
{
    public bool carrying;
    bool hasHungUp = false;
    public GameObject ring;
    public GameObject voiceOver;
    public GameObject raycast;
    public GameObject pickUp;
    public GameObject busyLine;
    public GameObject doorHinge;

    public AudioSource hungup;
    public AudioSource call;

    bool end = false;

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;
        
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
    }

    void Start()
    {

    }

    private void ActionButtonPress()
    {
        if (raycast.GetComponent<RayDanielScene>().raycollider == GetComponent<Collider>())
       {
            carrying = true;

            ring.GetComponent<AudioSource>().Pause();
            pickUp.GetComponent<AudioSource>().Play(0);
            if (hasHungUp == false)
            {
                voiceOver.GetComponent<AudioSource>().Play(0);
                hasHungUp = true;
            }
            else if (hasHungUp)
            {
                busyLine.GetComponent<AudioSource>().Play(0);

            }

      }

    }
    void Update()
    {

        if (!hungup.isPlaying && hasHungUp && !call.isPlaying && !end)
        {
            StartCoroutine(EndScene(3.0f));
            end = true;
        }
    }

    IEnumerator EndScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.EndScene();
    }
}
