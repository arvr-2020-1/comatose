﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HangUpScript : MonoBehaviour
{
    public GameObject phone;
    public GameObject voiceOver;
    public GameObject raycast;
    public GameObject hangUp;
    public GameObject busyLine;
    GameObject phoneTransform;

    private void Start()
    {
        phoneTransform = new GameObject();
        phoneTransform.transform.position = phone.transform.position;
        phoneTransform.transform.rotation = phone.transform.rotation;
        phoneTransform.transform.localScale = phone.transform.localScale;

    }

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
    }

    private void ActionButtonPress()
    {

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,out hit))
        {
            //Debug.Log(hit.collider.gameObject + " " + this);
            if (hit.collider.gameObject == this.gameObject)
            {
                phone.GetComponent<PhoneScript>().carrying = false;
                phone.GetComponent<Collider>().enabled = true;

                phone.transform.position = phoneTransform.transform.position;
                phone.transform.rotation = phoneTransform.transform.rotation;
                phone.transform.localScale = phoneTransform.transform.localScale;

                hangUp.GetComponent<AudioSource>().Play(0);
                voiceOver.GetComponent<AudioSource>().Pause();
                busyLine.GetComponent<AudioSource>().Stop();
                GetComponent<Collider>().enabled = false;
               
            }
        }

    }
}
