﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayDanielScene : MonoBehaviour
{

    private GameObject cameraController;
    public bool hitting;
    public Collider raycollider;


    void Start()
    {
 
        cameraController = Camera.main.gameObject;
    }

        void Update()
        {

        if (SceneController.IsPreparing)
            return;


        RaycastHit hit;

            Ray ray = new Ray(cameraController.transform.position, cameraController.transform.forward);

            Debug.DrawRay(cameraController.transform.position, cameraController.transform.forward);

            raycollider = null;

            if (Physics.Raycast(ray, out hit))
            {
                raycollider = hit.collider;    

                if (hit.collider.CompareTag("Item"))
                {
                   // hit.collider.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Highlight", 0.0f);
                    hitting = true;
                }

                else
                {
                    
                    hitting = false;
             
                }
            }
        }


}
