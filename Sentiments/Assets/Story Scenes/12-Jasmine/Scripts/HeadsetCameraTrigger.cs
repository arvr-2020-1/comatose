﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadsetCameraTrigger : MonoBehaviour
{

    Camera camera;

    void Start()
    {
        camera = GetComponent<Camera>();
        camera.enabled = false;
    }


    private void OnEnable()
    {
        //subscribe event for when it is time to disappear
        SceneController.sceneTransitionCallback += OnUnload;

    }

    void OnUnload()
    {
        Debug.Log("Screenshoting");
        camera.enabled = true;
        transform.SetParent(Camera.main.transform,false);
 
        StartCoroutine("disableCamera");
 
        //unsubscribe since it is not needed anymore (scene is going to be unloaded)
        SceneController.sceneTransitionCallback -= OnUnload;
    }

    IEnumerator disableCamera()
    {
       yield return new WaitForSeconds(0.01f);
      //  camera.enabled = false;
     //   Debug.Log("disabled");

    }

    void Update()
    {

    }
}