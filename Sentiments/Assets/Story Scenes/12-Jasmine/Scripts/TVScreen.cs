﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TVScreen : MonoBehaviour
{
    VideoPlayer videoPlayer;
    public GameObject TVBody;
    public GameObject TVLight;

    private void OnEnable()
    {
        SceneController.actionCallback += Click;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= Click;
    }

    void Click()
    {
        RaycastHit hit;

        if (Physics.Raycast(new Ray(Camera.main.transform.position, Camera.main.transform.forward), out hit))
        {
            if (hit.collider.CompareTag("Tv"))
            {
                videoPlayer.Stop();
                TVLight.SetActive(false);
            }
        }
    }


    void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }

    private void Update()
    {
        if (SceneController.IsPreparing)
            return;

        RaycastHit hit;


        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag("Tv"))
                TVBody.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 1.0f);
            else
                TVBody.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
        }
        else
            TVBody.GetComponent<MeshRenderer>().material.SetFloat("_Highlight", 0.0f);
    }
}
