﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabClean : MonoBehaviour
{
    //Game Object references
    GameObject playerCamera;
    float distance = 0.7f;

    Collider collider;
    Rigidbody rigidBody;
    public GameObject grip;
    public float TimeToEnd;

    bool grabbed;
    bool carrying;
    Material material;

    public GrabClean nextCarry;

    public bool endOnGrab = false;

    void Start()
    {
        grabbed = false;
        carrying = false;
        playerCamera = Camera.main.gameObject;
        rigidBody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        material = null;


        if (nextCarry) nextCarry.enabled = false;
        rigidBody.isKinematic = true;
        collider.enabled = true;

        if (grip == null)
            grip = new GameObject();

    }

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;

    }
    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;

    }


    // check when user committed action
    private void ActionButtonPress()
    {
        RaycastHit hit;

        if (!carrying)
        {
            Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.collider.name + " " + this.name);
                //if its  me
                if (hit.collider.name == this.name)
                {
                    collider.enabled = false;
                    if (!grabbed)
                    {
                        grabbed = true;
                        carrying = true;
                    }
                }
            }
        }

        else
        {
            if (!endOnGrab)//drop to floor
            {
                carrying = false;
                collider.enabled = true;
                rigidBody.isKinematic = false;
                if (nextCarry)
                    nextCarry.enabled = true;
            }
            else StartCoroutine("endScene",TimeToEnd);
        }



    }

    void GetMaterial()
    {
        material = GetComponent<MeshRenderer>().material;

    }


    void Update()
    {

        if (SceneController.IsPreparing)
            return;
        else if (material == null)
            GetMaterial();
            

        if (carrying)
        {
            transform.position = playerCamera.transform.forward * distance + playerCamera.transform.position + grip.transform.localPosition;
            transform.localRotation = playerCamera.transform.rotation * grip.transform.localRotation;
            GetComponent<Collider>().enabled = false;
        }

        RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {

            //if its  me
            if (hit.collider.name == this.name)
                material.SetFloat("_Highlight", 1.0f);
            else
                material.SetFloat("_Highlight", 0.0f);

        }
        else
            material.SetFloat("_Highlight", 0.0f);
    }

    IEnumerator endScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.EndScene();
        Debug.Log("end");

    }



}