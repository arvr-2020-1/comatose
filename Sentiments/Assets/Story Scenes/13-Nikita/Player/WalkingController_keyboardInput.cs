﻿/* 
 * author : jiankaiwang
 * description : The script provides you with basic operations of first personal control.
 * platform : Unity
 * date : 2017/12
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingController_keyboardInput : MonoBehaviour
{
    static Animator walkingAnimatorController;

    public float speed = 10f;
    private float translation;
    private float straffe;
    public GameObject headsetHead;
    public GameObject headsetHand;
    private GameObject[] shoes;
    bool headsetTriggered = false;
    bool headsetOff = false;
    
    void Awake ()
    {
        walkingAnimatorController = GetComponent<Animator>();

        //turn off the cursor
        Cursor.lockState = CursorLockMode.Locked;
        shoes = GameObject.FindGameObjectsWithTag("shoes");
        DisplayBody(false);
    }

    public void DisplayBody(bool state)
    {
        if (AppController.GetBuildType() != AppController.BuildType.AR)
        {
            if (!state)
            {
                shoes[0].SetActive(false);
                shoes[1].SetActive(false);
                headsetHand.SetActive(false);
                headsetHead.SetActive(false);
                headsetTriggered = false;
            }
            else
            {
                headsetHand.SetActive(true);
                headsetHead.SetActive(true);
                shoes[0].SetActive(true);
                shoes[1].SetActive(true);

                headsetTriggered = true;

            }
        }
    }
    public void EnableWalking()
    {
        if (AppController.GetBuildType() != AppController.BuildType.AR)
            walkingAnimatorController.SetBool("headsetOff", false);

    }
    public void RemoveHeadset()
    {
        if (AppController.GetBuildType() != AppController.BuildType.AR)
        {
            headsetHead.SetActive(false);
            if (headsetOff == false)
            {
                walkingAnimatorController.SetBool("headsetOff", true);
                headsetOff = true;
            }
        }     
    }

    //public void EnableHeadset()
  /*  void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Nikita" && headsetTriggered == false)
        {
            DisplayBody(true);
        }
    }
  */

void Update ()
        {
        // Input.GetAxis() is used to get the user's input
        // You can further set it on Unity. (Edit, Project Settings, Input)

        straffe = 0;
        translation = 0;

        walkingAnimatorController.SetBool("isBackward", false);
     walkingAnimatorController.SetBool("isForward", false);
        walkingAnimatorController.SetBool("isLeft", false);
    walkingAnimatorController.SetBool("isRight", false);    
        
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            translation =  -speed * .6f * Time.deltaTime;
            walkingAnimatorController.SetBool("isBackward", true);
        }
        
        else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            translation = speed * .6f * Time.deltaTime;
            walkingAnimatorController.SetBool("isForward", true);
        }
     
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            
            straffe =  -speed * .45f * Time.deltaTime;
            walkingAnimatorController.SetBool("isLeft", true);
        }
       else  if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            straffe = speed * .45f * Time.deltaTime;
            walkingAnimatorController.SetBool("isRight", true);
        }
       

        transform.Translate(straffe, 0, translation);

        if (Input.GetKeyDown("escape"))
        {
            // turn on the cursor
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
