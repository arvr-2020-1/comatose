﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NikitaWalkingScript : MonoBehaviour
{

    static Animator walkingAnimatorController;

    public float speed = .9f;
    private float translation;
    private float straffeTwo;
    public GameObject headsetHead;
    public GameObject headsetHand;
    public GameObject playerCamera;
    Vector3 lastPosition;
   
    // Start is called before the first frame update
    void Start()
    {
        walkingAnimatorController = GetComponent<Animator>();
        lastPosition = playerCamera.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        translation = Input.GetAxis("Vertical") * speed * .6f * Time.deltaTime;
        straffeTwo = Input.GetAxis("Horizontal") * speed * .3f * Time.deltaTime;
        transform.Translate(straffeTwo, 0, translation);


        Vector3 direction = playerCamera.transform.position - lastPosition;

        //float rb = playerCamera.GetComponent<Rigidbody>();

        float frontal = Vector3.Dot(direction, playerCamera.transform.forward);
        float straffe = Vector3.Dot(direction, playerCamera.transform.right);

        if (frontal < 0f)
        {
            walkingAnimatorController.SetBool("isBackward", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isBackward", false);
        }

        if (frontal > 0)
        {
            walkingAnimatorController.SetBool("isForward", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isForward", false);
        }

        if (straffe < 0)
        {
            walkingAnimatorController.SetBool("isLeft", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isLeft", false);
        }

        if (straffe > 0)
        {
            walkingAnimatorController.SetBool("isRight", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isRight", false);
        }

        if (Input.GetMouseButton(0))
        {
            walkingAnimatorController.SetBool("headsetOff", true);
            Destroy(headsetHead, 1);
            headsetHand.SetActive(true);
        }
        else
        {
            walkingAnimatorController.SetBool("headsetOff", false);
        }

        lastPosition = playerCamera.transform.position;

        Debug.Log(lastPosition);
    }
}
