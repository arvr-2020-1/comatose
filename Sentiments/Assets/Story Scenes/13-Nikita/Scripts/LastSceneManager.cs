﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class LastSceneManager : MonoBehaviour
{

    PostProcessVolume v;
  
    GameObject postProcessing;
    WalkingController_keyboardInput body;
    public float timeToEnd = 30f;
    public float timeToTalk = 40f;
    public float timeToRemove = 10f;

    public float animationDuration = 2f;
    float time;
    public GameObject doctorBehind;
    DoctorTalkingScript doctor;
    bool clicked = false;
    bool done = false;

    public enum Progression { WAITINGTOREMOVE, WAITINGTOTALK, WAITINGTOEND};
    public Progression progression;

    private void OnEnable()
    {
        SceneController.actionCallback += Click;
        body = Camera.main.transform.parent.GetComponent<WalkingController_keyboardInput>();
        if(body)
            body.DisplayBody(true);
    
    }
    private void OnDisable()
    {
        SceneController.actionCallback -= Click;
        if (postProcessing)
            postProcessing.SetActive(true);
    }

    void Click()
    {
      if(body)
        body.RemoveHeadset();
   
        time = Time.time;
        clicked = true;
        doctor.EnableTalk(); //do not let doctor talk until headset is removed
    }

    void Start()
    {
        done = false;
        clicked = false;
        doctor = doctorBehind.GetComponent<DoctorTalkingScript>();
        progression = Progression.WAITINGTOREMOVE;
        time = Time.time;
        postProcessing = GameObject.Find("Post-process Volume");
        if(postProcessing)
            postProcessing.SetActive(false);
    }

    private void Update()
    {
        //was waiting to remove, removed
        //cliked but wait for animation to finish
        if (progression == Progression.WAITINGTOREMOVE)
            if (clicked)
            {
                if ((Time.time - time) > animationDuration)
                {
                    //animation finished, enable walk
                    progression = Progression.WAITINGTOTALK;
                    if(body)
                        body.EnableWalking();            
                    time = Time.time;
                }
            }

        //was waiting for talk but has started talking
        //doctor is talking countdonw to end
        if(progression == Progression.WAITINGTOTALK)
            if(doctor.isTalking())
            {
                time = Time.time;
                progression = Progression.WAITINGTOEND;
            }

        //did not click but so much time has passed
        // remove the headset
        if (progression == Progression.WAITINGTOREMOVE)
            if ((Time.time - time) > timeToRemove)
                Click();

        //did not look at the doctor, start talking
        if (progression == Progression.WAITINGTOTALK)
            if ((Time.time - time) > timeToTalk)
            {
                doctor.Talk();
                time = Time.time;
                progression = Progression.WAITINGTOEND;
            }

        //end!
        if (progression == Progression.WAITINGTOEND)
            if ((Time.time - time) > timeToEnd)
            {
                if (!done)
                {
                    done = true;
                    SceneController.SceneReady();
                    SceneController.EndScene();
                }
            }
    }


}
