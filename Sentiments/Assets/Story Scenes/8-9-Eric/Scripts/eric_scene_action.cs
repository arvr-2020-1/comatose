﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eric_scene_action : MonoBehaviour

{

    public GameObject KidMomVoice;
    public GameObject KidBillVoice;
    public GameObject BillParticles;
    public GameObject bill;

    Carry billCarry;
    float time;
    int state;
    bool ended;

    // Start is called before the first frame update
    void Start()
    {

        billCarry = bill.GetComponent<Carry>();
        BillParticles.SetActive(false);
        KidMomVoice.SetActive(false); //mom sequence
        KidBillVoice.SetActive(true); //reminders active
        time = Time.time;
        state = 0;
        ended = false;

    }

    public void GrabbedBill()
    {
        time = Time.time;
        KidBillVoice.SetActive(false);
        state = 1;

    }

    public void DroppedBill()
    {
        state = 2;
        time = Time.time;
        BillParticles.SetActive(true);
        KidMomVoice.SetActive(true);
    }


    // Update is called once per frame
    void Update()
    {

        //wait for the scene to completely fade in, otherwise some items may stay invisible when altering materials
        if (SceneController.IsPreparing)
            return;

        //auto pick bill
        if ( ((Time.time - time) > 20) && (state == 0))
        {
            billCarry.carrying = true;
            KidBillVoice.SetActive(false);
            state = 1;
            time = Time.time;
        }

        //if too long after pick and do not drop
        if ((state ==1) && ((Time.time - time) > 10))
        {
            time = Time.time;
            state = 2;
            BillParticles.SetActive(true);
            KidMomVoice.SetActive(true);//kid says mom, mom, mom and triggers all the correspoinding triggers and booleans
        }

        if((state ==2) && ((Time.time - time) > 10))
            End();//discards paper and triggers all the correspoinding triggers and booleans

    }
  


    void End()
    {
        if (!ended)
        {
            SceneController.EndScene();
            ended = true;
        }
    }


 


}
