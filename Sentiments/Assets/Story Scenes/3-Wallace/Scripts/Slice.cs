﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slice : MonoBehaviour
{
    public GameObject original;
    public GameObject sliced;
    bool isCut;
   

    // Start is called before the first frame update
    void Start()
    {
        sliced.SetActive(false);
        isCut = false;
       
    }

    public int CutHalf()
    {
        if (!isCut)
        {
            original.SetActive(false);
            sliced.SetActive(true);
            sliced.transform.DetachChildren();
            return 1;
        }
        else return 0;

    }

    // Update is called once per frame
    void Update()
    {
   
        
    }
}
