﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
    public float shakeSpeed;
    public float shakeIntensity;
    public Vector3 originPos;
    public bool isShaking = true;
    public AudioSource sound;


    GameObject playerCamera;
    void Start()
    {
        originPos = transform.position;
        playerCamera = Camera.main.gameObject;

    }

    // Update is called once per frame
    void Update()
    { 
        if (SceneController.IsPreparing) //boxes can't shake if scene is loading
            return;

        RaycastHit hit;

        // create ray that matches player  head direction
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);

        //raycast the ray

        if (Physics.Raycast(ray, out hit))
        {
            //if its  me
            if (hit.collider.name == this.name)
            {

                float step = shakeSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, originPos + Random.insideUnitSphere * .01f, step);

                if(sound)
                { 
                    if (!sound.isPlaying)
                    {
                        sound.Play();
                    }
                }
            }
            else
            {
                if (sound.isPlaying)
                {
                    sound.Stop();
                }
            }
        }
    }
}






