﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTwoScript : MonoBehaviour

{

    private GameObject playerCamera;
    public SceneController controller;    
    public GameObject sceneTwoObjects;
    public GameObject[] stampedPapers;
    public GameObject heldStamper;
    public GameObject stamper;
    public GameObject[] stamperHighlight;
    private bool stampGrabbed; //boolean to check if we are currently stamping a paper
    private bool stampFinalGrabbed; //boolean to check if the stamp has been grabbed once
    public Material stampedMaterial;
    private int papersStamped;
    private Vector3 target;

    public AudioSource stampSound;



    // Start is called before the first frame update
    void Start()
    {
        stampGrabbed = false;
        stampFinalGrabbed = false;
        papersStamped = 0;
        playerCamera = Camera.main.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        //wait for the scene to completely fade in, otherwise some items may stay invisible when altering materials
        if (SceneController.IsPreparing)
            return;

        RaycastHit hit;

        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        target = Camera.main.transform.position - new Vector3(0, 0.25f, 0) + Camera.main.transform.forward * 0.7f;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Stamper") && !stampFinalGrabbed) //if we hit the stamper and have not grabbed the stamper once
            {
                highlight();
                if (SceneController.ActionButton) 
                { 
                    hit.collider.gameObject.GetComponent<RenderEnableDisable>().derenderObjects();
                    heldStamper.GetComponent<RenderEnableDisable>().renderObjects();
                    stampFinalGrabbed = true;
                    stampGrabbed = true;
                    dehighlight();
                }
            }
            else if (hit.collider.CompareTag("Paper") && stampGrabbed && papersStamped != 6) //if we hit the paper with the raycast and have the stamp grabbed
            {
                GameObject paperObject = hit.collider.gameObject;

                if (SceneController.ActionButton || ((heldStamper.transform.position.x < .28f && heldStamper.transform.position.x > .14f) &&
                        (heldStamper.transform.position.y < .81f) && (heldStamper.transform.position.z < -.71f && heldStamper.transform.position.z > -0.77f)))
                {

                    StartCoroutine(stampPaper(1.0f)); //start the coroutine
                    if (papersStamped == 5)
                    {
                        StartCoroutine(EndScene(2.0f));
                    }
                    stampSound.Play();
                }
            }
            else
            {
                dehighlight();
            }
        }
        Vector3 offset = new Vector3(0.1f, 0.5f, 0.6f);
        heldStamper.transform.position = Vector3.MoveTowards(heldStamper.transform.position, target, 0.5f);
    }

    //ends the scene after duration and stopps the office noise
    IEnumerator EndScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.EndScene();
    }

    IEnumerator stampPaper(float duration)
    {
        //ddrender the held stamper
        stampGrabbed = false;
        heldStamper.GetComponent<RenderEnableDisable>().derenderObjects();

        //unhighlight the paper
        //        paper.GetComponent<HighlightObject>().materialChange(false);

        //get the position and rotation of the paper

        //change the angel and position of the stamp

        //render the stamp on the paper
        stamper.GetComponent<RenderEnableDisable>().renderObjects();

        //pause the coroutine
        yield return new WaitForSeconds(duration);

        //derender the stamp on the paper
        stamper.GetComponent<RenderEnableDisable>().derenderObjects();

        stampedPapers[papersStamped].GetComponent<RenderEnableDisable>().renderObjects();

        //add to the amt of papers stamped
        papersStamped++;

        //rerender the stampped object
        stampGrabbed = true;
        heldStamper.GetComponent<RenderEnableDisable>().renderObjects();
    }

    private void highlight()
    {
        foreach (GameObject child in stamperHighlight)
        {
            child.GetComponent<Renderer>().sharedMaterial.SetFloat("_Highlight", 1.0f);
        }
    }

    private void dehighlight()
    {
        foreach(GameObject child in stamperHighlight)
        {
            child.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Highlight", 0.0f);
        }
    }
    /*
    IEnumerator moveClock(GameObject clock)
    {
        float duration = 3.0f; //Takes 5 seconds for light to turn on

        while (duration >= 0.0f)
        {
            clock.transform.Translate(Vector2.up * Time.deltaTime);
            clock.transform.Translate(Vector3.forward * Time.deltaTime);
            clock.transform.Rotate(Vector2.up, 10f);
        }

        return null;
    }
    */
}
