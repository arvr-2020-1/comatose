﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEditor;
using UnityEngine;

public class SceneOneScript : MonoBehaviour
{
    private GameObject playerCamera;
    private bool doneScene;
    private bool highlightedObject;
    public Light l;
    public GameObject[] clockParts;
    public SceneController controller;
 
    public AudioSource alarmOn;
    public AudioSource alarmOff;

    public VoiceSequence kid;

    private AudioSource[] alarm;
    bool ending = false;

    // Start is called before the first frame update
    void Start()
    {
        highlightedObject = false;
        doneScene = false;
        ending = false;
        playerCamera = Camera.main.gameObject;
        alarm = GetComponents<AudioSource>();
        alarmOn = alarm[0];
        alarmOff = alarm[1];
        alarmOn.Play();
        playerCamera = Camera.main.gameObject;      
    }

    // Update is called once per frame
    void Update()
    {
        //wait for the scene to completely fade in, otherwise some items may stay invisible when altering materials
        if (SceneController.IsPreparing || doneScene)
            return;

        RaycastHit hit;

        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Alarm") && !doneScene) //if we hit the alarm clock
            {
                highlightClock(l.intensity / 2.4f);
                highlightedObject = true;
                if (SceneController.ActionButton && !ending) //turn off the alarm clock
                {
                    alarmOn.Stop();
                    alarmOff.Play();
                    kid.Pause();
                    ending = true;
                    //hit.collider.GetComponent<AudioSource>().volume = 0;
                    StartCoroutine(EndScene(2.0f));
                    highlightedObject = false;
                    dehighlightClock();
                    doneScene = true;
                }
            }

            else if (highlightedObject)
            {
                dehighlightClock();
                highlightedObject = false;
            }
        }
        else if (highlightedObject)
        {
            dehighlightClock();
            highlightedObject = false;
        }
    }

    //ends the scene after duration and stopps the office noise
    IEnumerator EndScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        Debug.Log("ending");
        SceneController.EndScene();
    }

    private void dehighlightClock()
    {
        foreach (GameObject child in clockParts)
        {
            child.GetComponent<Renderer>().sharedMaterial.SetFloat("_Highlight", 0.0f);
        }
    }

    private void highlightClock(float multiplier)
    {
        foreach (GameObject child in clockParts)
        {
            child.GetComponent<Renderer>().sharedMaterial.SetFloat("_Highlight", 1.0f);
        }
    }
}
