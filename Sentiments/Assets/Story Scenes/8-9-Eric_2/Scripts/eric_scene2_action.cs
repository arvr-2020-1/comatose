﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eric_scene2_action : MonoBehaviour

{

    private GameObject playerCamera;
   /// private GameObject DirectionalLight;
    private GameObject Capsule;

    public GameObject Phone;

    private GameObject ParticleSystem;
    // private GameObject ReachableFloor;
    private GameObject KidsVoice;
    private GameObject KidsVoice2;

    AudioSource a_MyAudioSource;
    AudioSource b_MyAudioSource;


    private GameObject iPhoneAlarm;
    private GameObject BillParticles;


    private int stage = 1;
    private int counter = 0;

    private Light lt;

    // Start is called before the first frame update
    void Start()
    {

       /// Clicked = false;
      ///  phoneDark = Resources.Load("Phone_Screen1", typeof(Material)) as Material;
       ///   phoneReg = Resources.Load("Phone_Screen", typeof(Material)) as Material;


       // playerCamera = Camera.main.gameObject;

        ParticleSystem = GameObject.Find("ParticleSystem");
        KidsVoice = GameObject.Find("KidsVoice");
        KidsVoice2 = GameObject.Find("KidsVoice2");
        iPhoneAlarm = GameObject.Find("iPhoneAlarm");

        a_MyAudioSource = KidsVoice.GetComponent<AudioSource>();
        b_MyAudioSource = iPhoneAlarm.GetComponent<AudioSource>();
        a_MyAudioSource.volume = 1.0f;
        b_MyAudioSource.volume = 0.0f;

       // BillParticles = GameObject.Find("BillParticles");

        lt = GameObject.Find("eric_phoneglow").GetComponent<Light>();

        Phone.SetActive(false);
        ParticleSystem.SetActive(true);
        a_MyAudioSource.Play();
        KidsVoice2.SetActive(false);
        b_MyAudioSource.Play();
       // BillParticles.SetActive(true);

       ///// fades in the phone and phone light
      StartCoroutine("FadeInAudio");//Start a coroutine to run independent from the update function


    }



    ///// private bool Clicked = false;
    //Enables SceneController callbacks

    private void OnEnable()
    {
        SceneController.actionCallback += Click;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= Click;
    }

    void Click()
    {
        /////Clicked = true;

        if (stage == 2 || stage == 3)
        {

            DiscardPhone();//discards phone and triggers all the correspoinding triggers and booleans

        }
    }


 

    // Update is called once per frame
    void Update()
    {


        //wait for the scene to completely fade in, otherwise some items may stay invisible when altering materials
        if (SceneController.IsPreparing)
            return;


          /////////////
          ///////
          ///////
          //////////

             counter++;

          /////////////
          ///////
          ///////
          //////////

  
        if (counter >= 300 && stage ==2 )
        {
            KidsVoice2.SetActive(true);
            stage = 3;
            counter = 0;
        }



        if (counter >= 6000 && stage ==3 )
            DiscardPhone();//discards phone and triggers all the correspoinding triggers and booleans


       // Phone.transform.position = (playerCamera.transform.forward*0.4f) + playerCamera.transform.position;
       // Phone.transform.localRotation = playerCamera.transform.rotation;
      
    }
   

    void DiscardPhone()
    {  //Set the trigger at true so the gameobject can move to the right,the timer is at false and then the counter is reseted at 0.

        ///Debug.Log("end scene");
        ///Phone.SetActive(false);
       // ParticleSystem.SetActive(false);
      //  iPhoneAlarm.SetActive(false);
      //  KidsVoice2.SetActive(false);

        SceneController.EndScene();

    }



    IEnumerator FadeInAudio() 
    {
        float duration = 5.0f;//time you want it to run
        float interval = 0.0f;//interval time between iterations of while loop
        lt.intensity = 0.0f;

        float counter = 0f;
        while (counter < 5f)
        {

            a_MyAudioSource.volume -= 0.001f;
            b_MyAudioSource.volume += 0.001f;
            lt.intensity += 0.005f;
            counter += Time.deltaTime;
            yield return null;

        }

        Phone.SetActive(true);
        KidsVoice.SetActive(false);
       // BillParticles.SetActive(false);

        /////////
        ////////
        //////
        stage = 2;
        counter = 0;

       /// Debug.Log("faded in");
        yield break;
 }






    }
