﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoctorTalkingScript : MonoBehaviour

{
    private Animator animator;
    private GameObject playerCamera;
    bool talking = false;
    bool enableTalk = false;

    public bool isTalking()
    {
        return talking;
    }

    void Start ()
    {
        animator = GetComponent<Animator>();
       // animator.SetBool("IsPlayerLooking", false); //iddl
        playerCamera = Camera.main.gameObject;
    
    }
    public void EnableTalk()
    {
        enableTalk = true;
    }

    public void Talk()
    {
        animator.SetBool("IsPlayerLooking", true);
        talking = true;

    }


    private void Update()
    {
        RaycastHit hit;
        if (enableTalk)
        {
            Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.CompareTag("Item")) //if we hit the alarm clock
                {
                    this.transform.LookAt(new Vector3(playerCamera.transform.position.x,this.transform.position.y, playerCamera.transform.position.z));
                    Talk();
                }
            }
        }
    }
}
