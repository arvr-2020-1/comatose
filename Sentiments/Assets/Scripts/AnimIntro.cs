﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimIntro : MonoBehaviour
{

    public Material floorMaterial;
    float noise = 30f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnApplicationQuit()
    {
        floorMaterial.SetFloat("_NoiseScale", 30f);
    }

    // Update is called once per frame
    void Update()
    {
        noise += 0.005f;
        floorMaterial.SetFloat("_NoiseScale", noise);
    }
}
