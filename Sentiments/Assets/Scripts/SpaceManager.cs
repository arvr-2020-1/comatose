﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using System.Collections.Generic;
using UnityEngine;

/* 
 * WSL
 * Finds an estimative for wall positions and walkable area from scanning mesh
 */
public class SpaceManager : MonoBehaviour
{
	public GameObject floor;    //virtual world root to be aligned
	public bool debugMode = false;
	
	//used for hull calculation
	class Line
	{
		public Vector3 origin; public Vector3 target;
		public Line(Vector3 a, Vector3 b) { origin = a; target = b; }
	}

	//room processing steps
	enum Step { SAMPLE_WALLS, COMPUTE_WALLS, SAMPLE_FLOOR, COMPUTE_FLOOR, DONE, FAIL };

	Step step; //current step

	List<Vector3> pointList;
	List<Vector3> sampleList;
	List<Line> rayList;
	ConcaveHull hullWall;
	ConcaveHull hullFloor;
	private float floorLevel=10f;	//floor level in absolute world coordinates
	private float wallLevel = 0;
	Vector3 roomCenter;
	Vector3 longestDirection = Vector3.forward; //vector with the physical room major axis
	IReadOnlyList<IMixedRealitySpatialAwarenessMeshObserver> observers;
	public Vector3 GetRoomCenter() { return roomCenter; }
	public Vector3 GetLongestDirection () { return longestDirection; }

	int layerMask = 1 << 31; //only sample spatial mesh layer 
	bool finishedScanning = false;

	Vector3 corner1;
	Vector3 corner2;

	public GameObject forwardCollider;
	public GameObject backCollider;
	public GameObject rightCollider;
	public GameObject leftCollider;

	void Start()
	{
		step = Step.SAMPLE_WALLS;
		floorLevel = -0.3f;

		hullWall = new ConcaveHull();
		hullFloor = new ConcaveHull();
		sampleList = new List<Vector3>();
		pointList = new List<Vector3>();
		rayList = new List<Line>();

		//Debug.Log("initializing");

		finishedScanning = false;
		GetObservers();
	}

	private void GetObservers()
	{
		// Use CoreServices to quickly get access to the IMixedRealitySpatialAwarenessSystem
		var spatialAwarenessService = CoreServices.SpatialAwarenessSystem;

		// Cast to the IMixedRealityDataProviderAccess to get access to the data providers
		var dataProviderAccess = spatialAwarenessService as IMixedRealityDataProviderAccess;

		// Get the first Mesh Observer available, generally we have only one registered
		observers = dataProviderAccess.GetDataProviders<IMixedRealitySpatialAwarenessMeshObserver>();

		foreach (IMixedRealitySpatialAwarenessMeshObserver observer in observers)
		{
			observer.Suspend();
			observer.ClearObservations();
			observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.None;
		}
	}

	public void StopScanning()
	{
		foreach (IMixedRealitySpatialAwarenessMeshObserver observer in observers)
		{
			// Set to not visible
			observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.None;
			observer.Suspend();
		}
		finishedScanning = true;

		//Debug.Log("Stop scanning");
	}

	public void StartScanning()
	{
		finishedScanning = false;
		step = Step.SAMPLE_WALLS;
		//Debug.Log("Starting scanning");
		foreach (IMixedRealitySpatialAwarenessMeshObserver observer in observers)
		{
			// Set to not visible
			observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.Visible;
			observer.Suspend();
			observer.ClearObservations();
			observer.Resume();

		}

	}

	public bool Success()
    {
		if (step == Step.DONE)
			return true;
		else
			return false;

    }

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.KeypadEnter))
			ComputeBoundaries();

		if (debugMode)
			DrawDebugVisuals();

		if (finishedScanning)
			ComputeBoundaries();
	}

	void ComputeBoundaries()
	{


		{
			switch (step)
			{
				case Step.SAMPLE_WALLS:
					pointList.Clear();
					sampleList.Clear();
					wallLevel = Camera.main.transform.position.y;
					SampleWalls(ref pointList, Camera.main.transform.position);
					//Debug.Log(pointList.Count);
					if (pointList.Count > 50)
						step = Step.COMPUTE_WALLS;
					else
						step = Step.FAIL;
					break;

				case Step.COMPUTE_WALLS:
					hullWall.Generate(pointList, 2.5f);
					ComputeSamplePositions(hullWall, ref sampleList, wallLevel);
					step = Step.SAMPLE_FLOOR;
					break;

				case Step.SAMPLE_FLOOR:
					rayList.Clear();
					pointList.Clear();
					floorLevel = SampleFloors(sampleList, ref pointList, -0.5f);
					step = Step.COMPUTE_FLOOR;
					break;

				case Step.COMPUTE_FLOOR:
					hullFloor.Generate(pointList, 0.3f);
					floor.transform.Rotate(Vector3.up, Vector3.SignedAngle(Vector3.forward, longestDirection,Vector3.up), Space.World);
					floor.transform.position = new Vector3(roomCenter.x, floorLevel, roomCenter.z);
					MoveColliders(floor, hullWall);
					//Debug.Log("rotationg " + Vector3.Angle(longestDirection, Vector3.forward));
					roomCenter.y = floorLevel;
					foreach (IMixedRealitySpatialAwarenessMeshObserver observer in observers)
					{
						observer.Suspend();
						observer.ClearObservations();
					}
					step = Step.DONE;

					break;

			}
		}

	}

	void MoveColliders(GameObject root, ConcaveHull hull)
	{
		forwardCollider.transform.position = root.transform.position + 3 * root.transform.forward;
		backCollider.transform.position = root.transform.position - 3 * root.transform.forward;
		rightCollider.transform.position = root.transform.position + 3 * root.transform.right;
		leftCollider.transform.position = root.transform.position - 3 * root.transform.right;

		foreach (ConcaveHull.Edge e in hull.BorderEdges)
		{
			Vector3 A = new Vector3(e.A.x, 0, e.A.z);
			Vector3 B = new Vector3(e.B.x, 0, e.B.z);
			if (Math3d.AreLineSegmentsCrossing(root.transform.position, root.transform.position + 50 * root.transform.forward, A, B))
				forwardCollider.transform.position = (A + B) / 2.0f;
			

			if (Math3d.AreLineSegmentsCrossing(root.transform.position, root.transform.position - 50 * root.transform.forward, A, B))
				backCollider.transform.position = (A + B) / 2.0f;
	

			if (Math3d.AreLineSegmentsCrossing(root.transform.position, root.transform.position + 50 * root.transform.right, A, B))
				rightCollider.transform.position = (A + B) / 2.0f;
		

			if (Math3d.AreLineSegmentsCrossing(root.transform.position, root.transform.position - 50 * root.transform.right, A, B))
				leftCollider.transform.position = (A + B) / 2.0f;
			
				
		}


	}

	void DrawDebugVisuals()
	{
		//draw
		switch (step)
		{

			case Step.COMPUTE_WALLS:
				DrawRays(rayList);
				break;

			case Step.SAMPLE_FLOOR:
				DrawHull(hullWall, Color.cyan, wallLevel);
				break;

			case Step.COMPUTE_FLOOR:
				DrawRays(rayList);
				break;

			case Step.DONE:
				DrawHull(hullFloor, Color.magenta, floorLevel);
				DrawHull(hullWall, Color.cyan, wallLevel);
				break;

		}
	}

	//Sample 360 degrees around horizontally from origin
	//hopefully will hit only walls
	void SampleWalls(ref List<Vector3> hits, Vector3 origin)
	{
		RaycastHit hit;
		Quaternion q = new Quaternion();

		//Debug.Log("Sampling walls");

		
		for (float a = 0; a < 361; a += 5f)
		{
			q.eulerAngles = new Vector3(0, a, 0);
			Vector3 dir = q * Vector3.forward;
			rayList.Add(new Line(origin, origin + dir * 50f)); //fill raylist for drawing
			if (Physics.Raycast(new Ray(origin, dir), out hit, 50.0f, layerMask))
			{
				if( Vector3.Distance(hit.point,origin) < 4.0f)
					hits.Add(new Vector3(hit.point.x,0,hit.point.z));

			}
		}

	}

	//Using wall hull:
	//Compute room center and longest direction
	//Generate vertical positions for floor sampling at level height and return in list 
	void ComputeSamplePositions(ConcaveHull hull, ref List<Vector3> list, float level)
	{
		//Debug.Log("computing center and sample positions");
		Vector3 min;
		Vector3 max;

		min.x = 500; min.z = 500;
		max.x = -500; max.z = -500;
		ConcaveHull.Edge longestEdge = new ConcaveHull.Edge();

		// axis-aligned bounding box
		//one edge
		Vector3 minX = Vector3.zero;
		Vector3 minZ = Vector3.zero;
		//another edge
		Vector3 maxX = Vector3.zero;
		Vector3 maxZ = Vector3.zero;




		List<Vector3> boundary = new List<Vector3>();

		longestDirection = Vector3.zero;
		roomCenter = Vector3.zero;

		Vector3 first = hull.BorderEdges[0].A;
	     corner1 = first;
		float max_distance = 0;

		foreach (ConcaveHull.Edge e in hull.BorderEdges)
		{
			Vector3 A = new Vector3(e.A.x, 0, e.A.z);
			Vector3 B = new Vector3(e.B.x, 0, e.B.z);

			if(max_distance < (A-first).magnitude)
            {
				max_distance = (A - first).magnitude;
				corner1 = A;
            }


			boundary.Add(A);
			roomCenter += A;

			//find the extremes for the bounding box
			if (A.x < min.x)
			{
				minX = A;
				min.x = A.x;
			}
			if (A.x > max.x)
			{
				maxX = A;
				max.x = A.x;
			}

			if (A.z < min.z)
			{
				minZ = A;
				min.z = A.z;
			}

			if (A.z > max.z)
			{
				maxZ = A;
				max.z = A.z;
			}
			
		}

		 corner2 = corner1;
		max_distance = 0;

		foreach (ConcaveHull.Edge e in hull.BorderEdges)
		{
			Vector3 A = new Vector3(e.B.x, 0, e.B.z);

			if (max_distance < (A - corner1).magnitude)
			{
				max_distance = (A - corner1).magnitude;
				corner2 = A;
			}
		}

		//find room center
		roomCenter = (maxX + maxZ + minX + minZ)/ 4.0f;
		//roomCenter /= hull.BorderEdges.Count;


		//longestDirection = FindLongestAxis(boundary, new Vector3(roomCenter.x, 0, roomCenter.z));

		longestDirection =  (corner2 - corner1).normalized; //diagonal -> find out how much to rotate later

		//find the longest vector that connects two points that touch
		//the axis-aligned bounding box. This is the longest edge that should
		//be rotate to align the room
		/*	Vector3 edge1 = minY - minX;
			Vector3 edge2 = maxX - maxY;
			if (edge1.magnitude > edge2.magnitude)
				longestDirection = minY - minX;
			else
				longestDirection = maxY - maxX;
				*/


		//randomly create sample points inside box
		//scales with length to keep approximate density the same
		float area = Mathf.Abs(max.x - min.x) * Mathf.Abs(max.z - min.z);
		for (int i = 0; i <  100*area; i++)		
			list.Add(new Vector3(Random.Range(min.x, max.x), level, Random.Range(min.z, max.z)));



	}

	Vector3 FindLongestAxis(List<Vector3> boundary,Vector3 center)
	{
		float sumXX = 0;
		float sumZZ = 0;
		float sumXZ = 0;

		foreach (Vector3 v in boundary)
		{
			sumXX += (v.x - center.x) * (v.x - center.x);
			sumZZ += (v.z - center.z) * (v.z - center.z);
			sumXZ += (v.x - center.x) * (v.z - center.z);
		}

		float covxx = sumXX / boundary.Count;
		float covzz = sumZZ / boundary.Count;
		float covxz = sumXZ / boundary.Count;

		//cov matrix
		// covxx covxy
		// covxy covyy

		float trace = covxx + covzz;
		float det = covxx * covzz - covxz * covxz;

		//eigenvalue
		float lambda1 = (trace + Mathf.Sqrt(trace * trace - 4 * det)) / 2f;
		float lambda2 = (trace - Mathf.Sqrt(trace * trace - 4 * det)) / 2f;

		//eigenvectors
		Vector3 v1 = new Vector3(covxz,0, lambda1 - covxx);
		Vector3 v2 = new Vector3(covxz,0,lambda2 - covxx);

		v1.Normalize();
		v2.Normalize();


		//if (lambda1 > lambda2)
	//		return v1;
		//else
			return v2;
		}


	//Use samplelist to sample the mesh vertically 
	//fill hits with intersection points that are at least lower then upperFloorEstimative
	//return floor level
	float SampleFloors(in List<Vector3> samplelist, ref List<Vector3> hits, float upperFloorEstimative)
	{
		//Debug.Log("Sampling floor");
		float floor = upperFloorEstimative;

		List<float> floorlist = new List<float>();
		List<Vector3> roomHits = new List<Vector3>();

		//roomCenter = Vector3.zero;
		//check all sample points inside the room
		//
		foreach (Vector3 pos in samplelist)
		{
			RaycastHit hit;

			if (Physics.Raycast(new Ray(pos, -Vector3.up), out hit, 5.0f, layerMask))
			{
				//if (Vector3.Distance(hit.point, roomCenter) < 3.0f)
				{
					rayList.Add(new Line(hit.point, hit.point + Vector3.up * 0.10f)); //ONLY FOR DRAWING
					roomHits.Add(hit.point);
					//roomCenter += new Vector3(hit.point.z,0,hit.point.)

					//if posible floor add height to floor list
					if (hit.point.y < upperFloorEstimative)
						floorlist.Add(hit.point.y);
				}
			}
		}


		//compute again the axis using all points that hit the room
		Vector3 longestDirection2 = FindLongestAxis(roomHits, new Vector3(roomCenter.x, 0, roomCenter.z));

		if (Vector3.Dot(longestDirection2, longestDirection) > 0)
			longestDirection = Quaternion.Euler(0, 45, 0) * longestDirection;
		else
			longestDirection = Quaternion.Euler(0, -45, 0) * longestDirection;

		//combine with the previous estimative based on the walls
		//longestDirection = longestDirection2;
		//longestDirection /= 2.0f;

		//compute 30% trimmed means to find floor position
		floorlist.Sort();
		int cut = Mathf.RoundToInt(floorlist.Count * 0.3f);
		for (int i = cut; i< floorlist.Count-cut; i++)
		{
			floor += floorlist[i];
		}
		floor /= (floorlist.Count - 2 * cut);

		//Debug.Log("floor level is " + floor);

		//go through the list again and
		//only add the positions smaller than 30cm of the floor
		//for computing hull
		foreach (Vector3 pos in roomHits)
		{
			if (pos.y < (floor + 0.3f))
				hits.Add(pos);
		}


		roomCenter.y = floor;

		return floor;
	}

	void DrawRays(List<Line> lines)
	{
		foreach (Line l in lines)
			Debug.DrawLine(l.origin, l.target, Color.blue);

	}

	void DrawHull(ConcaveHull hull, Color c, float level)
	{
		if(hull.BorderEdges != null)
			foreach (ConcaveHull.Edge e in hull.BorderEdges)
			{
				Vector3 A = new Vector3(e.A.x, level, e.A.z);
				Vector3 B = new Vector3(e.B.x, level, e.B.z);
				Debug.DrawLine(A, B, c);

				//Debug.DrawLine(roomCenter, A, Color.green);
			}

		foreach (Vector3 r in pointList)
		{
			DrawCross(r, Color.yellow);

		}

		DrawCross(corner1, Color.red);
		DrawCross(corner2, Color.green);


		Debug.DrawLine(roomCenter, roomCenter + longestDirection * 10f, Color.white);

	}

	void DrawCross(Vector3 v, Color c)
    {
		Debug.DrawLine(v + Vector3.forward * 0.02f, v - Vector3.forward * 0.02f,c);
		Debug.DrawLine(v + Vector3.right * 0.02f, v - Vector3.right * 0.02f,c);
	}

}
