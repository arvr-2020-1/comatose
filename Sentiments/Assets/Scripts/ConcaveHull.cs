﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcaveHull
{ 
    public class Edge
    {
        public Vector3 A { get; set; }
        public Vector3 B { get; set; }
    }

    public ConcaveHull() { }

    public List<Edge> BorderEdges { get; private set; }

    public void Generate(List<Vector3> points, float alpha)
    {
        // 0. error checking, init
        if (points == null || points.Count < 2) { Debug.Log("AlphaShape needs at least 2 points"); }
        BorderEdges = new List<Edge>();
        float alpha_2 = alpha * alpha;

        // 1. run through all pairs of points
        for (int i = 0; i < points.Count - 1; i++)
        {
            for (int j = i + 1; j < points.Count; j++)
            {
                
                if (points[i] == points[j]) { 
                    Debug.Log("AlphaShape needs pairwise distinct points"); 
                } // alternatively, continue

                //make sure y coordinate is ignored
                float dist = Vector3.Distance(new Vector3(points[i].x, 0, points[i].z), new Vector3(points[j].x, 0, points[j].z));

                if (dist > 2 * alpha) { continue; } // circle fits between points ==> p_i, p_j can't be alpha-exposed                    

                float x1 = points[i].x, x2 = points[j].x, z1 = points[i].z, z2 = points[j].z; // for clarity & brevity

                Vector3 mid = new Vector3((x1 + x2) / 2, 0, (z1 +z2) / 2);

                // find two circles that contain p_i and p_j; note that center1 == center2 if dist == 2*alpha
                Vector3 center1 = new Vector3(
                    mid.x + (float)Mathf.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (z1 - z2) / dist,
                    0,
                    mid.z + (float)Mathf.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (x2 - x1) / dist
                    
                    );

                Vector3 center2 = new Vector3(
                    mid.x - (float)Mathf.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (z1 - z2) / dist,
                    0,
                    mid.z - (float)Mathf.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (x2 - x1) / dist                   
                    );

                // check if one of the circles is alpha-exposed, i.e. no other point lies in it
                bool c1_empty = true, c2_empty = true;
                for (int k = 0; k < points.Count && (c1_empty || c2_empty); k++)
                {
                    if (points[k] == points[i] || points[k] == points[j]) { continue; }

                    if ((center1.x - points[k].x) * (center1.x - points[k].x) + (center1.z - points[k].z) * (center1.z - points[k].z) < alpha_2)
                    {
                        c1_empty = false;
                    }

                    if ((center2.x - points[k].x) * (center2.x - points[k].x) + (center2.z - points[k].z) * (center2.z - points[k].z) < alpha_2)
                    {
                        c2_empty = false;
                    }
                }

                if (c1_empty || c2_empty)
                {
                    // yup!
                    BorderEdges.Add(new Edge() { A = points[i], B = points[j] });
                }
            }
        }
    }

}
