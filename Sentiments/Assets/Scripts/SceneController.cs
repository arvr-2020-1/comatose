﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


//Move from one scene to the next
//Must exist in hierarchy
//Abstract input from controller and mouse/keyboard
public class SceneController : MonoBehaviour
{

    enum SceneState {INTRO, SCAN, LOADING, PREPARING, ACTIVE,UNLOADING};

    static SceneState sceneState;

    private int nextScene;
    static private bool actionButtonPressed = false;
    //singleton
    static SceneController sceneController = null;
    static private bool scenePreparing;

    public TextMeshPro log;
    public GameObject menuObject;
    bool cutEnabled = true;

    //public interface
    static public bool IsPreparing { get { if (sceneState == SceneState.ACTIVE) return false; else return true; } }
    static public bool ActionButton { get { return actionButtonPressed; } }
    public delegate void ActionButtonDownCallback();
    public static event ActionButtonDownCallback actionCallback;

    public delegate void ActionButtonEndCallback();
    public static event ActionButtonEndCallback actionEndCallback;

    //editor interface
    [SerializeField] public float timeToUnload;
    [SerializeField] public string[] sceneNames;
    public GameObject intro; //objects for the intro scene
    public AudioClip introMusic;
    public AudioClip creditMusic;

    public AudioSource effectAudioSource;
    public AudioSource musicAudioSource;
    float lastSceneTime;

    //public WalkingController_keyboardInput walkingController_temporary;


    public delegate void SceneTransitionCallback();
    public static event SceneTransitionCallback sceneTransitionCallback;

    public delegate void ScenePreLoadCallback(GameObject root);
    public static event ScenePreLoadCallback scenePreLoadCallback;
    public GameObject blackCanvas;
    GameObject playerCamera;

    //Menu Items -> Move later to another class

    enum MenuButton { NONE, START, TEAM};

    MenuButton menuSelected;

    public MenuOption start;
    public MenuOption team;
 
    private void OnEnable()
    {
        sceneController = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        nextScene = 0;
        timeToUnload = 1.0f;
        scenePreparing = false;
        cutEnabled = true;
        sceneState = SceneState.INTRO;
        musicAudioSource.clip = introMusic;
        musicAudioSource.Play();
        playerCamera = Camera.main.gameObject;

        lastSceneTime = Time.time;

        menuObject.transform.parent.GetComponent<Rigidbody>().isKinematic = true;
        menuObject.transform.parent.GetComponent<Rigidbody>().useGravity = false;
        menuObject.transform.parent.GetComponent<Carry>().carrying = false;

        if (AppController.GetBuildType() == AppController.BuildType.AR)
        {
            menuObject.SetActive(false);
            menuObject.transform.parent.position = new Vector3(menuObject.transform.parent.position.x,playerCamera.transform.position.y, menuObject.transform.parent.position.z);
        }


    }


    private void Update()
    {
        RaycastHit hit;
        if (!sceneController.menuObject.activeSelf)
            return;

        if(Physics.Raycast(new Ray(playerCamera.transform.position, playerCamera.transform.forward),out hit))
        {
            switch(hit.collider.transform.gameObject.name)
            {
                case "Start":                    
                    if (menuSelected != MenuButton.START)
                    {
                        start.Highlight();
                        effectAudioSource.Play();
                    }
                    menuSelected = MenuButton.START;
                    if (actionButtonPressed)
                    {
                        DisableMenu();
                        sceneState = SceneState.ACTIVE;
                        sceneController.AdvanceScene();
                        StartCoroutine("FadeAudio",musicAudioSource);
                    }
                    break;

                case "Team":
                    if (menuSelected != MenuButton.TEAM)
                    {
                        effectAudioSource.Play();
                        team.Highlight();
                    }
                    menuSelected = MenuButton.TEAM;
                    if(actionButtonPressed)
                    {
                        cutEnabled = false;
                        intro.SetActive(false);
                        sceneState = SceneState.ACTIVE;
                        nextScene = sceneNames.Length-1;
                        sceneController.AdvanceScene();

                    }

                    break;

            }

        }
        else
        {
            menuSelected = MenuButton.NONE;
            if (sceneController.menuObject.activeSelf)
            {
                start.Reset();
                team.Reset();
            }
        }


    }
    IEnumerator FadeAudio(AudioSource audio)
    {
        float d = audio.volume / 20f;
        while (audio.volume > d)
        {
            audio.volume -= d;
            yield return new WaitForSeconds(0.5f);
        }
        audio.Stop();

    }
    public static void PlayCreditMusic()
    {
        sceneController.musicAudioSource.clip = sceneController.creditMusic;
        sceneController.musicAudioSource.Play();
    }

    public static void EnableMenu()
    {
        sceneController.menuObject.SetActive(true);

    }

    public static void DisableMenu()
    {
        sceneController.menuObject.SetActive(false);

    }

    public static void SetMessage(string msg)
    {

        sceneController.log.SetText(msg);
    }


    public static void ActionStart()
    {
        actionButtonPressed = true;
        actionCallback?.Invoke();

    }

    public static void ActionEnd()
    {
        actionButtonPressed = false;
        actionEndCallback?.Invoke();

    }


    //Interface called when others want to advance the scene
    public static void EndScene()
    {
        //ignore super fast advances
        if( (Time.time - sceneController.lastSceneTime) > 5)
        {
            sceneController.lastSceneTime = Time.time;
            sceneController.AdvanceScene();
        }
    }

    public static void NewSceneLoaded(GameObject sceneRoot)
    {
        sceneState = SceneState.PREPARING;
        scenePreLoadCallback?.Invoke(sceneRoot);
    }

    //new scene has finished transition and is now visible
    public static void SceneReady()
    {
       sceneState = SceneState.ACTIVE;

    }


    //called when 
    //when scene has finished fading out and is ready for unload
    public static void SceneDismiss()
    {
        sceneController.Unload();
    }


    void AdvanceScene()
    {

        if (sceneState != SceneState.ACTIVE)
            return;

        if (nextScene < sceneNames.Length)
        {
            SceneManager.LoadSceneAsync(sceneNames[nextScene], LoadSceneMode.Additive);
            Debug.Log("Loading" + sceneNames[nextScene]);
            
            sceneState = SceneState.LOADING;

            //do not unload the first scene (just hide)
            if (nextScene == 0)
            {
                intro.SetActive(false);
            }

            //send signal for fadeout
            //check a way for only the old scene receive this
            sceneTransitionCallback?.Invoke();

            nextScene++;

   
        }

       else if (nextScene == sceneNames.Length) //if last scene, teleport
        {
            menuObject.SetActive(true);
            intro.SetActive(true);
            cutEnabled = true;
            nextScene++;
            Unload();
            nextScene = 0;


        }
        
    }

    public static void  Black(bool state)
    {
        if (!sceneController)
            return;

        if(state && sceneController.cutEnabled)
        {
            sceneController.blackCanvas.transform.SetParent(Camera.main.gameObject.transform, false);
            sceneController.blackCanvas.transform.localPosition = new Vector3(0, 0, Camera.main.nearClipPlane + 0.002f);
            sceneController.blackCanvas.SetActive(true);

        }
        else
            sceneController.blackCanvas.SetActive(false);

        
    }

    void Unload()
    {
        if (nextScene > 1)
        {
            Debug.Log("unloading" + sceneNames[nextScene - 2]);
            sceneState = SceneState.UNLOADING;
            SceneManager.UnloadSceneAsync(sceneNames[nextScene - 2], UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }
    }


}
