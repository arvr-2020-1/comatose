﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : MonoBehaviour
{
    public float length;
    public float secondsPerUpdate;
    public Material[] materials;
    bool visible;
    public GameObject sceneRoot;
    public Color ambientColor = Color.white;
    public float ambientIntensity = 1.0f;
    //Material fullScreenQuadMaterial;
    bool loaded;

    public enum TransitionType { NONE, DISSOLVE, CUT};
    public TransitionType transitionTypeIn = TransitionType.DISSOLVE;
    public TransitionType transitionTypeOut = TransitionType.DISSOLVE;


    void OnApplicationQuit()
    {
        //reset the materials to good state.
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_DissolveTime", -1.5f);
            materials[i].SetFloat("_Highlight", 0f);
            materials[i].SetColor("_EdgeColor", new Color(0, 690, 1024f));
            materials[i].SetColor("_OutlineColor", new Color(0, 690, 1024f));
        }
    }
    private void Start()
    {
        
    }

    //register a callback from when this scene finish loading
    private void OnEnable()
    {
        //subscribe so that we know once scene has loaded completely
        SceneManager.sceneLoaded += OnSceneLoaded;

        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_DissolveTime", 1.5f);
            materials[i].SetFloat("_Highlight", 0f);
            materials[i].SetColor("_EdgeColor",new Color(0, 690, 1024f));
            materials[i].SetColor("_OutlineColor", new Color(0, 690, 1024f));
        }
        visible = false;
        loaded = false; //used to ensure loaded callback is called only once. 
        length = 5f;
        secondsPerUpdate = 0.01f;
        SceneController.Black(false);

       // if (AppController.GetBuildType() == AppController.BuildType.AR)
     /*   {
            RenderSettings.ambientSkyColor = ambientColor* ambientIntensity;
            RenderSettings.ambientEquatorColor = ambientColor* ambientIntensity; 
            RenderSettings.ambientIntensity = ambientIntensity;
            RenderSettings.ambientLight = ambientColor;
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
        }
     */
    }

    //scene has finished loading warn the sceneController
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (loaded) //ensure this is called only once, ideally it would be better to check using the scene name.
            return;

        loaded = true;

        SceneController.NewSceneLoaded(sceneRoot);

        switch(transitionTypeIn)
        {
            case TransitionType.DISSOLVE:
                DissolveIn();
                break;
            case TransitionType.CUT:
                CutToFromBlackReady(true);
                break;
      
            case TransitionType.NONE:
                Debug.Log("none load ");
                visible = true;
                SceneController.SceneReady();
                break;
        }


        //subscribe event for when it is time to disappear
        SceneController.sceneTransitionCallback += OnUnload;
    }

    //called by scenecontroller when this scene should transiton
    void OnUnload()
    {

        switch (transitionTypeOut)
        {
            case TransitionType.DISSOLVE:
                DissolveOut();
                break;
            case TransitionType.CUT:
                CutToFromBlackReady(false);
                break;
        
            case TransitionType.NONE:
                visible = false;
                SceneController.SceneDismiss();
                Debug.Log("none unload " + SceneManager.GetActiveScene().name);
                break;
        }


    }

    //scene is unloaded unregister the event
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        //unsubscribe since it is not needed anymore (scene is going to be unloaded)
        SceneController.sceneTransitionCallback -= OnUnload;
    }

    public void CutToFromBlackReady(bool ready)
    {
        SceneController.Black(true);
        //fullScreenQuadMaterial.SetColor("_BaseColor", new Color(0,0,0,1));
        Debug.Log("toblack " + SceneManager.GetActiveScene().name);

        if (ready)
        {
            visible = false;
            StartCoroutine("WaitReady", length);
        }
        else
        {
            visible = true;
            StartCoroutine("WaitDismiss", length);

        }
    }

    IEnumerator WaitDismiss(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.Black(false);
        SceneController.SceneDismiss();
    }

    IEnumerator WaitReady(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.Black(false);
        SceneController.SceneReady();
    }

    public void DissolveOut()
    {
        StartCoroutine(Animate(length,-1.5f,1.5f));
    }

    public void DissolveIn()
    {
        StartCoroutine(Animate(length, 1.5f, -1.5f));
    }

    IEnumerator Animate(float duration,float start, float end)
    {
        float startTime = Time.time;

        while ((Time.time - startTime) < duration)
        {
            for (int j = 0; j < materials.Length; j++)
                materials[j].SetFloat("_DissolveTime", start + (end-start)/duration * (Time.time - startTime));
            yield return new WaitForSeconds(secondsPerUpdate);
        }
        visible = !visible;

        if (visible)
            SceneController.SceneReady();

        if(!visible)
            SceneController.SceneDismiss();
    }

}
