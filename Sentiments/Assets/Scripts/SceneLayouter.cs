﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.LWRP;

public class SceneLayouter : MonoBehaviour
{
    public SpaceManager spaceManager;
    GameObject oo;

    // Start is called before the first frame update
    void Start()
    {
        oo = null;
    }

    // Update is called once per frame
    void Update()
    {
            
    }

    public void Layout(GameObject o)
    {
        

        oo = o;
        o.transform.Rotate(Vector3.up, Vector3.SignedAngle(Vector3.forward, spaceManager.GetLongestDirection(), Vector3.up), Space.World);
        o.transform.position = spaceManager.GetRoomCenter();
    }

    private void OnDrawGizmos()
    {
        if (oo == null)
            return;


        GameObject child;
        GameObject direction;
        for (int i = 0; i < oo.transform.childCount; i++)
        {
            child = oo.transform.GetChild(i).transform.gameObject;
            if (child.name == "ALL")
            {           
                for (int j = 0; j < child.transform.childCount; j++)
                {
                    direction = child.transform.GetChild(j).transform.gameObject;
                    if (direction.name == "Forward")
                        direction.transform.position = new Vector3(spaceManager.forwardCollider.transform.position.x,direction.transform.position.y, spaceManager.forwardCollider.transform.position.z);
                    if (direction.name == "Back")
                        direction.transform.position = new Vector3(spaceManager.backCollider.transform.position.x, direction.transform.position.y, spaceManager.backCollider.transform.position.z);
                    if (direction.name == "Right")
                        direction.transform.position = new Vector3(spaceManager.rightCollider.transform.position.x, direction.transform.position.y, spaceManager.rightCollider.transform.position.z);
                    if (direction.name == "Left")
                        direction.transform.position = new Vector3(spaceManager.leftCollider.transform.position.x, direction.transform.position.y, spaceManager.leftCollider.transform.position.z);


                    //                        Transform c = child.transform.GetChild(j).transform;
                    //                       Debug.DrawLine(c.position, spaceManager.GetRoomCenter(), Color.red);
                }

            }
            
        }

    }

}
