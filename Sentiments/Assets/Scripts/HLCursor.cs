﻿using Microsoft.MixedReality.Toolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HLCursor : MonoBehaviour
{
    
    private GameObject cursor =null;

    // Start is called before the first frame update
    void Start()
    {
        cursor = Camera.main.gameObject.transform.parent.transform.Find("DefaultCursor(Clone)").GetChild(0).gameObject;

    }

    // Update is called once per frame
    void Update()
    {
        if(cursor!=null)
            if (cursor.activeSelf == false)
               cursor.SetActive(true);
    }
}
