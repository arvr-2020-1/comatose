﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Credits : MonoBehaviour
{
    // Start is called before the first frame update

    float time=0;
    WalkingController_keyboardInput body;
    bool endScene = false;
    public float interval = 3f;
    public float startEnd = 3f;
    int index;


    [System.Serializable]
    public struct RoleName {public string role; public string name; }
    public RoleName [] roleList;
    public TextMeshPro role;
    public TextMeshPro person;
    public bool start;
    
    void Start()
    {
        start = false;
        index = 0;
        endScene = false;
        time = Time.time;
        body = Camera.main.transform.parent.GetComponent<WalkingController_keyboardInput>();
    
        SceneController.PlayCreditMusic();
    }

    private void OnEnable()
    {
        SceneController.actionCallback += Click;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= Click;
    }

    void Click()
    {
        endScene = true;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Time.time - time) > startEnd)
        {
            start = true;
            time = Time.time;
        }

        if (start)
        {
            if ((Time.time - time) > interval)
            {
                if (index < roleList.Length)
                {
                    role.text = roleList[index].role;
                    person.text = roleList[index].name;
                    time = Time.time;
                    index++;

                }
               
            }
        }

        if(endScene)
        {
            Debug.Log("ending last scene");
            SceneController.EndScene();
            if(body)
                body.DisplayBody(false);
        }
    }
}
