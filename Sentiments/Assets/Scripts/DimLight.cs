﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DimLight : MonoBehaviour
{
    public Light lt;
    public float waitTime = 2.0f;
    public float ARIntensity;
    public float targetIntensity;
    float startTime;
    public float transitionTime = 5f;
    bool turnOn;

    void Start()
    {
        if (AppController.GetBuildType() == AppController.BuildType.AR)
            targetIntensity = ARIntensity;
        else
            targetIntensity = lt.intensity;

        lt.intensity = 0;
        turnOn = false;
        Invoke("TurnLightOn", waitTime);//Invoke certain function after 3.0f seconds
        startTime = Time.time;
    }

    void TurnLightOn()
    {
        turnOn = true;
        startTime = Time.time;
    }

    private void Update()
    {
        if (turnOn)
        {
            if (lt.intensity < targetIntensity)
                lt.intensity = (targetIntensity / transitionTime) * (Time.time - startTime - waitTime);
            else
                turnOn = false;
        }
    }

    
}
