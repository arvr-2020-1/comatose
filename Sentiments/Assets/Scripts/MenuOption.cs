﻿using Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOption : MonoBehaviour
{
    public GameObject bar;
    public float minimumScale;
    public float maxScale;
    Material barMaterial;

    // Start is called before the first frame update
    void Start()
    {
        barMaterial = bar.GetComponent<MeshRenderer>().material;
        barMaterial.SetColor("_EdgeColor", new Color(255, 255, 255));
    }

    public void Highlight()
    {
        barMaterial.SetColor("_EdgeColor", new Color(0, 690, 1024f));
    }

    public void Reset()
    {
        barMaterial.SetColor("_EdgeColor", new Color(255, 255, 255));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
