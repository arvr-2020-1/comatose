﻿using Microsoft.MixedReality.Toolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Carry : MonoBehaviour
{
    //Game Object references
    GameObject playerCamera;
    float distance = 0.8f;

    Collider collider;
    Rigidbody rigidBody;
    Renderer renderer;

    bool grabbed;
    public bool carrying = false;
    Material material;

    float obstacleDistance;

    public Carry nextCarry;
    public UnityEvent onGrab;
    public UnityEvent onDrop;
    public GameObject grip;
    public Material highlightMaterial;
    public bool endOnGrab = false;
    public bool canDrop = true;
    public AudioSource grabSound;
    public float timeToEnd=5;
    public float speed = 2f;

    void Start()
    {
        grabbed = false;
        obstacleDistance = 10f;
        playerCamera = Camera.main.gameObject;
        rigidBody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        material = null;

        renderer = GetComponent<Renderer>();

        if (nextCarry) nextCarry.enabled = false;
        rigidBody.isKinematic = true;
        rigidBody.useGravity = false;
        collider.enabled = true;

        if (carrying)
        {
            grabbed = true;
            carrying = true;
            rigidBody.isKinematic = false;
            rigidBody.useGravity = false;
        }

        if (grip == null)
            grip = new GameObject();

    }

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;



    }
    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;

    }


    // check when user committed action
    private void ActionButtonPress()
    {
        RaycastHit hit;

        if (!carrying)
        {
            Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
            if (Physics.Raycast(ray, out hit))
            {
                //if its  me
                if (hit.collider.name == this.name)
                {
                    //collider.enabled = false;
                    if (!grabbed)
                    {
                        if (highlightMaterial)
                            renderer.material = material;
    
                        if(material)
                            material.SetFloat("_Highlight", 0.0f);

                        if (onGrab !=  null) onGrab.Invoke();
                        grabbed = true;
                        carrying = true;
                        rigidBody.isKinematic = false;
                        rigidBody.useGravity = false;

                        if (grabSound) grabSound.Play();
                    }
                }
               
            }
        }

        else 
        {
            carrying = false;
            rigidBody.isKinematic = false;
            grabbed = false;

            if (onDrop != null) onDrop.Invoke();

            if (canDrop)
                rigidBody.useGravity = true;
            else
                rigidBody.isKinematic = true;

            if (!endOnGrab)//drop to floor
            { 
                if (nextCarry)
                    nextCarry.enabled = true;
            }
            else
            {
                StartCoroutine("endScene", timeToEnd);
            }
        }



    }


    void GetMaterial()
    {
        material = GetComponent<MeshRenderer>().material;

    }


    void Update()
    {

        if (SceneController.IsPreparing)
            return;

        else if (material == null)
            GetMaterial();

        RaycastHit hit;
        Ray ray;

        if (!carrying)
            ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        else
            ray = new Ray(this.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
         {              
                //if its  me
                if (hit.collider.name == this.name)
                {
                    if (!carrying)
                    {
                        if (highlightMaterial)
                            renderer.material = highlightMaterial;
                        material.SetFloat("_Highlight", 1.0f);
                    }
                }
                else
                {
                    if (!carrying)
                    {
                        if (highlightMaterial)
                            renderer.material = material;
                        material.SetFloat("_Highlight", 0.0f);
                    }
                    else
                        obstacleDistance = hit.distance;
         
                }
          }
          else
            {
                if (highlightMaterial)
                    renderer.material = material;
                material.SetFloat("_Highlight", 0.0f);
            }

      

    }

    void FixedUpdate()
    {
        if (carrying)
        {
            Vector3 df = (playerCamera.transform.position - this.transform.position);
            distance = df.magnitude;
            rigidBody.isKinematic = false;

            GameObject anim = grip.transform.GetChild(0).transform.gameObject;

            Vector3 pMount = (playerCamera.transform.forward * 0.5f + playerCamera.transform.position);
            //Debug.DrawLine(playerCamera.transform.position, pMount, Color.red);

            Vector3 pGrip = pMount - playerCamera.transform.rotation * (grip.transform.localRotation * Vector3.Scale(grip.transform.localPosition, grip.transform.parent.localScale));
            //Debug.DrawLine(pMount, pGrip, Color.green);

            Vector3 f = pGrip - playerCamera.transform.rotation * grip.transform.localRotation * (anim.transform.localRotation * anim.transform.localPosition);
            //Debug.DrawLine(pGrip, f, Color.blue);

            rigidBody.velocity = -speed * (this.transform.position - pGrip);
            transform.rotation = playerCamera.transform.rotation * grip.transform.localRotation * anim.transform.localRotation;
   

        }



    }

    IEnumerator endScene (float duration)
	{
        yield return new WaitForSeconds(duration);
        Debug.Log("end");
        SceneController.EndScene();
	}



}