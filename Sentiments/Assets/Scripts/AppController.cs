﻿using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR;


public class AppController : MonoBehaviour
{
    private SpaceManager spaceManager;
    private SceneLayouter sceneLayouter;

    public GameObject   spaceManagerObj;
    public GameObject scanButton;
    public GameObject floorObj;
    TextMeshPro text;

    bool ready = false;

    private InputDevice leftController;
    private InputDevice rightController;
    public enum BuildType { PC, AR, VR};

    private static AppController appController=null;

    public bool scanning = false;
    bool clicked = false;


    public  BuildType buildType;

    private bool triggerButtonRight = false;
    private bool primaryButtonRight = false;

    private bool oldTriggerButtonRight = false;
    private bool oldPrimaryButtonRight = false;

    private bool triggerButtonLeft = false;
    private bool primaryButtonLeft = false;

    private bool oldTriggerButtonLeft = false;
    private bool oldPrimaryButtonLeft = false;

    public static BuildType GetBuildType()
    {
        if (appController == null)
            return BuildType.PC;
        else
            return appController.buildType;
    }

    private void OnEnable()
    {
        SceneController.actionCallback += Click;

        appController = this;
        if (buildType == BuildType.AR)
        {
            text = scanButton.GetComponent<TextMeshPro>();
            scanButton.SetActive(true);
            floorObj.SetActive(false);
        }
        else
        {
            if(scanButton)
                scanButton.SetActive(false);
            if(floorObj)
                floorObj.SetActive(true);
        }
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= Click;

    }

    void Click()
    {
        clicked = true;

    }

    bool FindControllers()
    {
        var leftDevices = new List<UnityEngine.XR.InputDevice>();
        var rightDevices = new List<UnityEngine.XR.InputDevice>();

        InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.RightHand, rightDevices);
        InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.LeftHand, leftDevices);

        SceneController.SetMessage("Looking for controllers");
     
        InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.RightHand, rightDevices);
        InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.LeftHand, leftDevices);

            if (leftDevices.Count >= 1)
            {
                leftController = leftDevices[0];
                SceneController.SetMessage("Click to start.");
                //Debug.Log(string.Format("Device name '{0}' with role '{1}'", leftController.name, leftController.role.ToString()));
            }

            if (rightDevices.Count >= 1)
            {
                rightController = rightDevices[0];
                SceneController.SetMessage("Click to start.");
                //Debug.Log(string.Format("Device name '{0}' with role '{1}'", rightController.name, rightController.role.ToString()));
            }


        if (leftDevices.Count >= 1 || leftDevices.Count >= 1)
            return true;
        else
            return false;

    }

    // Start is called before the first frame update
    //Get the devices required for each input

    void Start()
    {
        if (buildType == BuildType.AR)
        {
            spaceManager = spaceManagerObj.GetComponent<SpaceManager>();
            sceneLayouter = spaceManagerObj.GetComponent<SceneLayouter>();
            SceneController.scenePreLoadCallback += PrepareScene;

         //   PointerUtils.SetHandPokePointerBehavior(PointerBehavior.AlwaysOff, Handedness.Any);
         //   PointerUtils.SetHandRayPointerBehavior(PointerBehavior.AlwaysOff);
        }
        
        else if(buildType == BuildType.VR)
        {
            FindControllers();
        }

    }

    private void OnDestroy()
    {
        SceneController.scenePreLoadCallback -= PrepareScene;
    }

    public void PrepareScene(GameObject root)
    {
      
        GameObject child;
        for (int i = 0; i < root.transform.childCount; i++)
        {
            child = root.transform.GetChild(i).transform.gameObject;
            child.SetActive(false);
            if (buildType == BuildType.AR)
            {
                if (child.name == "ALL" || child.name == "AR")
                    child.SetActive(true);
            }
            else if(buildType == BuildType.VR)
            {
                if (child.name == "ALL" || child.name == "VR")
                    child.SetActive(true);

            }
        }
   
        sceneLayouter.Layout(root);

    }

    public void HoloLensPointerDown()
    {
        SceneController.ActionStart();
    }

    public void HoloLensPointerUp()
    {
        SceneController.ActionEnd();
    }

    // Update is called once per frame
    void Update()
    {

        //check if can start
        if (!ready)
        {
            if (buildType == BuildType.VR)
            {
                if (FindControllers())
                    ready = true;
            }
        }

        //mouse input
        if (buildType == BuildType.PC )
        {
            if (Input.GetMouseButtonDown(0))
                SceneController.ActionStart();

            if (Input.GetMouseButtonUp(0))
                SceneController.ActionEnd();

            //advance scene manually
            if (Input.GetKeyDown("space"))
            {
                SceneController.EndScene();
            }

        }

        //controller input
        if (buildType == BuildType.VR)
        {
            if (rightController!= null)
            {
                rightController.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out primaryButtonRight);   //advance scene debug 
                rightController.TryGetFeatureValue(CommonUsages.triggerButton, out triggerButtonRight);          //click items in game
            }
 
            if(leftController != null)
            {
                leftController.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out primaryButtonLeft);   //advance scene debug 
                leftController.TryGetFeatureValue(CommonUsages.triggerButton, out triggerButtonLeft);          //click items in game
            }


            //transition to pressed
            if ( (!oldTriggerButtonRight && triggerButtonRight) || (!oldTriggerButtonLeft && triggerButtonLeft))
            {
                SceneController.ActionStart();
            }

            if ((oldTriggerButtonRight && !triggerButtonRight) || (oldTriggerButtonLeft && !triggerButtonLeft))
            {
                SceneController.ActionEnd();
            }

            //advance scene manually
            if ( (primaryButtonRight && !oldPrimaryButtonRight) || (primaryButtonLeft && !oldPrimaryButtonLeft))
            {
                SceneController.EndScene();
                //Debug.Log("callingendinse");
            }

            oldTriggerButtonRight = triggerButtonRight;
            oldPrimaryButtonRight = primaryButtonRight;

            oldTriggerButtonLeft = triggerButtonLeft;
            oldPrimaryButtonLeft = primaryButtonLeft;
        }
        
        //Tap input
        if (buildType == BuildType.AR)
        {
                if (Input.GetKeyDown(KeyCode.Y))
                {
                    spaceManager.StopScanning();
                }

                if (Input.GetKeyDown(KeyCode.T))
                {
                    spaceManager.StartScanning();
                }
        }

        if (buildType == BuildType.AR)
        {
            RaycastHit hit;
            if (clicked)
            {
                clicked = false;
                if (Physics.Raycast(new Ray(Camera.main.transform.position, Camera.main.transform.forward), out hit))
                {
                    
                    switch (hit.collider.transform.gameObject.name)
                    {
                        case "Scan":
                            if (scanning == false)
                            {
                                spaceManager.StartScanning();
                                SceneController.DisableMenu();
                                scanning = true;
                                text.text = "Stop";
                            }
                            else
                            {
                                spaceManager.StopScanning();                           
                                scanning = false;
                                text.text = "Scan";
                            }
                            break;


                    }

                }
            }

            if(spaceManager.Success())
            {
                SceneController.EnableMenu();
            }
        }
    }
}
