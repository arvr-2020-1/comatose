﻿using System.Collections;
using UnityEngine;


public class VoiceSequence : MonoBehaviour
{
    public float initialWait;
    public AudioClip[] clips;
    public float[] interval;
    public int attemptsBeforeEnd=10;

    public float loopInterval;
    public AudioClip  [] audioLoop;

    
    AudioSource audioSource;
    bool pause;
    float startTime;
    int audioIndex;
    int attempts;
    float originalVolume;
    bool ended;

    // Start is called before the first frame update
    void Start()
    {
        ended = false;
        pause = false;
        audioIndex = 0;
        startTime = Time.time;
        attempts = 0;
        audioSource = GetComponent<AudioSource>();
        originalVolume = audioSource.volume;
    }

    private void OnDisable()
    {
        if(audioSource)
            audioSource.Stop();
    }

    public void Continue()
    {
        pause = false;
        audioSource.volume = originalVolume;
    }

    public void Pause()
    {

        StartCoroutine("FadeAudio", audioSource);
        pause = true;

    }
    IEnumerator FadeAudio(AudioSource audio)
    {
        float d = audio.volume / 10f;
        while (audio.volume > d)
        {
            audio.volume -= d;
            yield return new WaitForSeconds(0.5f);
        }
        audio.Stop();

    }
    // Update is called once per frame
    void Update()
    {
        if (pause)
        {
            startTime = Time.time;
            return;
        }
        if (!ended)
        {
            if (attempts > attemptsBeforeEnd)
            {
                Debug.Log("Voice Sequence Ending");
                SceneController.EndScene();
                ended = true;
            }
            if (audioIndex < clips.Length)
            {
                if ((Time.time - startTime - initialWait) > interval[audioIndex])
                {
                    audioSource.PlayOneShot(clips[audioIndex]);
                    startTime = Time.time;
                    audioIndex++;
                    attempts++;
                }
            }
            else
            {
                if (audioLoop.Length > 0)
                {
                    if ((Time.time - startTime - initialWait) > loopInterval)
                    {
                        audioSource.PlayOneShot(audioLoop[Random.Range(0, audioLoop.Length)]);
                        startTime = Time.time;
                        attempts++;
                    }
                }
            }
        }
    }
}
