﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RaycastAction : MonoBehaviour
{

    private GameObject playerCamera;
    public GameObject doorHit;
    public GameObject knobHit;
    public Door doorscript;
    public Knob knobscript;
    float knobTurned;
    public AudioClip doorSound;
    public AudioClip knobSound;
    public AudioClip openDoorSound;
    public AudioClip closeDoorSound;
    public AudioClip slamDoorSound;
    public AudioSource Audio;
    public bool dooropen;

    // Start is called before the first frame update
    void Start()
    {
  
        Audio = doorHit.GetComponent<AudioSource>();
        dooropen = false;
        Audio.PlayOneShot(slamDoorSound, 0.7f);
        playerCamera = Camera.main.gameObject;


    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit hit;

        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Door"))
            {
                knobHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
                hit.collider.gameObject.GetComponent<MeshRenderer>().material.color = new Color32(0, 255, 0, 255);
                doorHit = hit.collider.gameObject;

                if (SceneController.ActionButton && knobTurned >= 3 && dooropen == false)
                {
                    doorscript.OnPress();
                    dooropen = true;
                    Audio.PlayOneShot(openDoorSound, 0.7f);
                }
                else if (SceneController.ActionButton && knobTurned >= 3 && dooropen == true)
                {
                    dooropen = false;
                    doorscript.closedoor();
                    Audio.PlayOneShot(closeDoorSound, 0.7f);
                    StartCoroutine(EndScene());
                }
                else if (SceneController.ActionButton)
                {
                    Audio.PlayOneShot(doorSound, 0.7f);
                }

            }

            else if (hit.collider.CompareTag("Knob"))
            {
                doorHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
                hit.collider.gameObject.GetComponent<MeshRenderer>().material.color = new Color32(0, 255, 0, 255);
                knobHit = hit.collider.gameObject;

                if (SceneController.ActionButton)
                {
                    knobscript.OnPress();

                    knobTurned = knobTurned + 1;

                    Audio.PlayOneShot(knobSound, 0.7f);
                }
            }

            else if (hit.collider.CompareTag("KnobCollider"))
            {
                doorHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
                knobHit.GetComponent<MeshRenderer>().material.color = new Color32(0, 255, 0, 255);

                if (SceneController.ActionButton)
                {
                    knobscript.OnPress();

                    knobTurned = knobTurned + 1;

                    Audio.PlayOneShot(knobSound, 0.7f);
                }

            }
            else if (hit.collider.CompareTag("Item"))
            {
                doorHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
                knobHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
            }

            //lastObjectHit.GetComponent<MeshRenderer>().material.color = new Color32(100, 100, 100, 100);
            else
            {
                doorHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);
                knobHit.GetComponent<MeshRenderer>().material.color = new Color32(255, 255, 255, 255);

            }

        }
    }

    IEnumerator EndScene()
    {
        yield return new WaitForSeconds(3);
        Debug.Log("Next Scene");
        SceneController.EndScene();
    }
         
}
    

