﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballSound : MonoBehaviour
{
    public AudioSource ballSoundOnCollision;
    bool played;
    bool doorOpened = false;

    void Start()
    {
        played = false;
    }

    void Update()
    {
        if (this.GetComponent<Transform>().position.y < 0.1f)
        {
            doorOpened = true;
        }

        
    }

    void OnTriggerEnter(Collider collider)
    {
        if (played == false)
        {
            if (doorOpened == true)
            {
                ballSoundOnCollision.Play();
                played = true;
            }
        }
    }   
}