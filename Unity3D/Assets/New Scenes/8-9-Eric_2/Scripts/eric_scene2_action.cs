﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eric_scene2_action : MonoBehaviour

{

    private Camera playerCamera;
    private GameObject DirectionalLight;
    private GameObject Capsule;
   /// private GameObject eric_walls;
    private GameObject Phone;
   // private GameObject Paper;
    private GameObject ParticleSystem;
   // private GameObject ReachableFloor;
    private GameObject KidsVoice;
    private GameObject iPhoneAlarm;
    private GameObject capsuleObject = null;
    private GameObject BillParticles;
    private GameObject eric_phonescreen;



    ////public Texture billHighlight;
    private Material billHighlight;
    private Material billReg;
    private Material phoneDark;
    private Material phoneReg;

    Vector3 targetPosition;

    private float movementTime = 1f;
    private bool done = true;
    private bool aiming = false;
    private bool paperhit = true;
    private bool phonehit = false;

    private int stage = 1;
    private int counter = 0;

 private Light lt;


    // Start is called before the first frame update
    void Start()
    {
 

        phoneDark = Resources.Load("Phone_Screen1", typeof(Material)) as Material;
          phoneReg = Resources.Load("Phone_Screen", typeof(Material)) as Material;

         ///playerCamera = GameObject.Find("playerCamera");
          /////////////  playerCamera = GameObject.Find("MainCamera");
         //////// playerCamera = Camera.main.gameObject;
        playerCamera = Camera.main;

          
          DirectionalLight = GameObject.Find("DirectionalLight");
         // Paper = GameObject.Find("Paper");
        ///  eric_walls = GameObject.Find("eric_walls");
          Phone = GameObject.Find("Phone");
          ParticleSystem = GameObject.Find("ParticleSystem");
         // ReachableFloor = GameObject.Find("ReachableFloor");
          KidsVoice = GameObject.Find("KidsVoice");
          iPhoneAlarm = GameObject.Find("iPhoneAlarm");
          iPhoneAlarm = GameObject.Find("eric_phonescreen");




          BillParticles = GameObject.Find("BillParticles");


          ////Light lt = GameObject.Find("eric_phoneglow");
         lt = GameObject.Find("eric_phoneglow").GetComponent<Light>();


         //// billHighlight = Texture.Find("billHighlight");

          Phone.SetActive(false);
        //  eric_walls.SetActive(false);
          DirectionalLight.SetActive(false);
          ParticleSystem.SetActive(true);
         // ReachableFloor.SetActive(false);
          KidsVoice.SetActive(true);
          iPhoneAlarm.SetActive(true);
          BillParticles.SetActive(true);

 



 


    }
 


 

 void TriggerFunction() {
     StartCoroutine("FadeIn");//Start a coroutine to run independent from the update function
 }


    // Update is called once per frame
    void Update()
    {




    



//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
/////// set raycaster to the player camera
//////////////////////////////////////////////////////////////////////

      RaycastHit hit;
      Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

      /////////////////////////////////////////////////////////////////////    
      /////////////////////////////////////////////////////////////////////
      ////// set raycaster to the player camera
      //////////////////////////////////////////////////////////////////////

      targetPosition.x = playerCamera.transform.position.x;
      targetPosition.y = playerCamera.transform.position.y -.2f;
      ////  targetPosition.z = playerCamera.transform.position.z - .2f;
      targetPosition.z = playerCamera.transform.position.z - .2f;


            /////////////////////////////////////////////////////////////////////
           ////// Highlight paper by looking at it
           //////////////////////////////////////////////////////////////////////

           if ( Physics.Raycast (ray,out hit)) 
           {
            }

          /////////////
          ///////
          ///////
          //////////

             counter++;

          /////////////
          ///////
          ///////
          //////////
             if (counter >= 600 && stage ==1 )
                  {
 

                      FadeInPhone();//discards phone and triggers all the correspoinding triggers and booleans

                  }

 
        


 /*

*/
 
             if (counter >= 6000 && stage ==2 )
                  {
 

                      DiscardPhone();//discards phone and triggers all the correspoinding triggers and booleans

                  }


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
/////// Interacting with objects on Button Action
//////////////////////////////////////////////////////////////////////

 if (SceneController.ActionButton)       

      {

        //////////
        ////////
        //////////////////////////////////////////////////////////////////////
        //// raycaster hit
        //////////////////////////////////////////////////////////////////////

        if ( Physics.Raycast (ray,out hit)) 
        {

        ///check intersection
       Debug.Log("You selected the " + hit.transform.name); 

   
          //////////////////////////////////////////////////////////////////////
       //////////////////////////////////////////////////////////////////////
       //// paper collider
       //////////////////////////////////////////////////////////////////////
/*
      if(hit.collider.CompareTag("Paper"))

          {
 

               ///////////////////////////////////////////////////////////////
              //// after the paper has been picked up hit state and booleans
              ///////////////////////////////////////////////////////////////


          if (paperhit==true)

              {

      

                      DiscardPaper();//gets rid of paper and swaps for phone and triggers all the correspoinding triggers and booleans
              }

          }
    
 */

               //////////
               ////////
              ///////////////////////////////////////////////////////////////
               //// The phone collider activators
              ///////////////////////////////////////////////////////////////



          if(hit.collider.CompareTag("Phone"))
            {

                if(phonehit==true) {

                      DiscardPhone();//discards phone and triggers all the correspoinding triggers and booleans

                }

             } 
          }

        }
 

 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
  /// updating the position of the paper and phone with the playerCamera
 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
/*
if (done==false) 
          {

 
 
            Paper.transform.position = (playerCamera.transform.forward*0.45f) + playerCamera.transform.position;
            Paper.transform.localScale = new Vector3(0.44f, 0.6f, 0.60506f);
            Paper.transform.localRotation = Quaternion.Euler((playerCamera.transform.eulerAngles.x+15.0f), playerCamera.transform.eulerAngles.y, playerCamera.transform.eulerAngles.z);
           
           //////////////////////////////////////////////////////////////////////////////////
           /////// old positioning without rotation /////////////////////////////////////////
           ////////////  Paper.transform.localRotation = playerCamera.transform.rotation; 
           //////////////////////////////////////////////////////////////////////////////////

            paperhit = true;


           }
*/

if (paperhit==true) 
          {

            Phone.transform.position = (playerCamera.transform.forward*0.4f) + playerCamera.transform.position;
            Phone.transform.localRotation = playerCamera.transform.rotation;

           }
        
        }
   

 

  
 




    void FadeInPhone()
    {  //get rid of the paper,  the counter is reset to 0.

            
             ///////////////////////////////////////////////////////////////
            ///// discard paper boolean controls
             ///////////////////////////////////////////////////////////////
    Invoke("TriggerFunction", 0f);//Invoke certain function after 3.0f seconds

 
 //////           ReachableFloor.SetActive(false);
            ParticleSystem.SetActive(true);
            DirectionalLight.SetActive(false);
            Phone.SetActive(true);
        ////    eric_walls.SetActive(false);
//////            Paper.SetActive(false);
            KidsVoice.SetActive(false);
            BillParticles.SetActive(false);
 
            phonehit=true;

                      /////////
                      ////////
                      //////
                        stage = 2;
                        counter = 0;

            //      eric_phonescreen.GetComponent<Renderer>().material = phoneReg;


    }




    void DiscardPhone()
    {  //Set the trigger at true so the gameobject can move to the right,the timer is at false and then the counter is reseted at 0.

            
             ///////////////////////////////////////////////////////////////
            ///// discard phone boolean controls
             ///////////////////////////////////////////////////////////////
      Debug.Log("phone"); 


              DirectionalLight.SetActive(true);
              Phone.SetActive(false);
              ParticleSystem.SetActive(false);
              ////// ReachableFloor.SetActive(true);
          iPhoneAlarm.SetActive(false);

               ////////////////////////////////
              /////////// advance scene
              /////////// with scene controller
              /////////////////////////////////
              SceneController.EndScene();
              /////////////////////////////////


                      /////////
                      ////////
                      //////
                        counter = 0;

    }



 IEnumerator FadeIn() {
     float duration = 5.0f;//time you want it to run
     float interval = 0.0f;//interval time between iterations of while loop
     lt.intensity = 0.0f;
  ///   Phone.opacity = 0.0f;



            float counter = 0f;
      while (counter < 5f){
         lt.intensity += 0.005f;
      ///  Phone.opacity += 0.005f;
           counter += Time.deltaTime;
           ///yield return 0;
                    yield return null;

      }



     yield break;
 }






    }
