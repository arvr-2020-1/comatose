﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastRachel : MonoBehaviour
{
    // Start is called before the first frame update
    private Camera playerCamera;

    public string hitObject;

   RaycastHit hit;



    public int speed=2;
    public int redCol;
    public int greenCol;
    public int blueCol;
    public bool lookingAtObject = false;
    public bool flashingIn = true;
    public bool startedFlashing = false;


    public GameObject RachelFurniture;
    public GameObject RachelWalls;
    public GameObject floor;
    public GameObject trigger;

    public GameObject drawer;

    public GameObject alarmclock;
    public GameObject piggybank;
    public bool hitSnooze = false;
    public bool drawerDone = false;
    public bool piggyBroke = false;
    public bool readyToBreak = false;
    public bool thrown = false;
    public bool smashed = false;



    Vector3 targetPosition;
    Vector3 targetPositionCoins;
 

    AudioSource Alarmbeep;
    AudioSource drawerNoise;
    Animation drawerOpen;

    public bool canHold = true;
    public GameObject item;
    public GameObject tempParent;
    public bool isHolding = false;

    public GameObject Brokenpiggy;
    public GameObject coins_container;

    AudioSource coins;
    AudioSource breaking;


    void Start()
    {
        
        Brokenpiggy.SetActive(false);
        coins_container.SetActive(false);
        piggybank.SetActive(false);

        drawer.SetActive(false);

        floor.SetActive(false);
        RachelWalls.SetActive(false);
        RachelFurniture.SetActive(false);
        trigger.SetActive(true);


        Alarmbeep = alarmclock.GetComponent<AudioSource>();
        drawerOpen = drawer.GetComponent<Animation>();
        drawerNoise = drawer.GetComponent<AudioSource>();

        breaking = Brokenpiggy.GetComponent<AudioSource>();
        coins = coins_container.GetComponent<AudioSource>();



     ////   playerCamera = Camera.main.gameObject;
        playerCamera = Camera.main;








    }









    void Update()
    {
      //////RaycastHit hit;
      Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        //Debug to show the actual ray in Scene
        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);


      /////////////////////////////////////////////////////////////////////    
      /////////////////////////////////////////////////////////////////////
      ////// set raycaster to the player camera
      //////////////////////////////////////////////////////////////////////

       targetPosition.y = floor.transform.position.y;
       targetPositionCoins.y = floor.transform.position.y;

      ////  targetPosition.z = playerCamera.transform.position.z - .2f;
 


            /////////////////////////////////////////////////////////////////////
           ////// Highlight paper by looking at it
           //////////////////////////////////////////////////////////////////////


        if (Physics.Raycast(ray, out hit))
        {

            /////////if(hit.collider.CompareTag("Paper"))

            hitObject = hit.transform.gameObject.name;
            


/*
            // testing Unhightlight on scroll over
            //if (hit.collider.CompareTag("Untagged"))
            {
                
                

                
                


               // Unhighlight();
                // need to add info for last select here?
                //Highlight();
                
                //lastSelect = hitObject;



                //if (hitObject != lastSelect)
                {
                   // Unhighlight();
                }

                // Step 1: turn off the alarm clock


            }

            //else
            
            {
                
                // hit an object without the trigger tag
                //Unhighlight();


                

            }
 
*/           if (hitSnooze == false)
            {
               

                if(hit.collider.CompareTag("alarm_rachel"))
                {

                ///// if (hitObject == "alarmclock")
                ///// {

                    Highlight();
                }
                
                
                //Debug.Log("this shoudl worki guess");
                
                
/*
                // action: click alarmclock
               ////// if (hitObject == "alarmclock" && SceneController.ActionButton == true)
                if (hitObject == "alarmclock" && SceneController.ActionButton == true)
                {
                    
                    hitSnooze = true;
                    Alarmbeep.Stop();
                    Unhighlight();
                    Debug.Log("alarm clock turned off");
                }

*/


            }
            
            // 2: Open the drawer
             if (drawerDone == false && hitSnooze == true)
            {

                if(hit.collider.CompareTag("drawer_rachel"))
                {

               ///// if (hitObject == "drawer")
               ///// {
                    Highlight();
                }
                

                    //Animation.Play("drawerOpen");
/*
                // action: open drawer (sound and animation)
                if (hitObject == "drawer" && SceneController.ActionButton == true)
                {
                    drawerOpen.Play();
                    drawerNoise.Play();
                    Unhighlight();
                    Debug.Log("opening drawer");
                    drawerDone = true;

                }
*/
            }

            // 3: Break the piggybank
             if (piggyBroke == false && hitSnooze == true)
            {
                if(hit.collider.CompareTag("piggy_rachel"))
                {
                
                ///// if (hitObject == ("piggybank"))
                ///// {

                    Highlight();
                }

/*                
                if (hitObject == "piggybank" && SceneController.ActionButton == true)
                {
                    Unhighlight();
                    piggyBroke = true;

                }
                if (piggyBroke == true)
                {
                    SceneController.EndScene();
                    Debug.Log ("Scene Ended");
                }
*/
            }



            // 4: select broken pieces
             if (smashed == true)
            {
                if(hit.collider.CompareTag("floor_rachel"))
                {
                
                ///// if (hitObject == ("piggybank"))
                ///// {

                    Highlight();
                }

 
            }




        }
        
        


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
/////// Interacting with objects on Button Action
//////////////////////////////////////////////////////////////////////

 if (SceneController.ActionButton)       

      {

        //////////
        ////////
        //////////////////////////////////////////////////////////////////////
        //// raycaster hit
        //////////////////////////////////////////////////////////////////////

        if ( Physics.Raycast (ray,out hit)) 
        {




       //////////////////////////////////////////////////////////////////////
       //////////////////////////////////////////////////////////////////////
       //// alarm collider
       //////////////////////////////////////////////////////////////////////

      if(hit.collider.CompareTag("alarm_rachel"))

          {
 
/////////////////////////////////////////////////////////////////
///////// turn off alarm ////////////////////////
////////////////////////////////////////////////////////////////





        if (hitSnooze==false) 
      
            {

        //////////////////////////////////////////////////////////////////////
        ///////// turn off alarm clock ///////////////////////////////////////
        //////////////////////////////////////////////////////////////////////


                    hitSnooze = true;
                    Alarmbeep.Stop();
                    Unhighlight();
                    /// Debug.Log("alarm clock turned off");

                    piggybank.SetActive(true);
                    floor.SetActive(true);
                     RachelWalls.SetActive(true);
                    RachelFurniture.SetActive(true);
                    trigger.SetActive(false);
                    drawer.SetActive(true);


            }


          }
    

                   
              ///////////////////////////////////////////////////////////////
               //// open drawer
              ///////////////////////////////////////////////////////////////



          if(hit.collider.CompareTag("drawer_rachel"))
            {



        if (drawerDone==false && hitSnooze==true) 
      
            {
                    
                    /// Animation.Play("drawerOpen");
                    drawerOpen.Play();
                    drawerNoise.Play();
                    Unhighlight();
                    // Debug.Log("opening drawer");
                    drawerDone = true;


            }

             } 



      //////////////////////////////////////////////////////////////////////
       //////////////////////////////////////////////////////////////////////
       //// piggy collider
       //////////////////////////////////////////////////////////////////////

      if(hit.collider.CompareTag("piggy_rachel"))

          {
 
/////////////////////////////////////////////////////////////////
///////// picking up piggy ////////////////////////
////////////////////////////////////////////////////////////////

       ///// if (aiming==false && done==false) 
        if (piggyBroke == false && hitSnooze == true  && isHolding==false) 
            {

        //////////////////////////////////////////////////////////////////////
        ///////// move the piggy to the playerCamera ////////////////////////
        //////////////////////////////////////////////////////////////////////



                     PickUpPiggy();//picks up piggy and triggers all the correspoinding triggers and booleans

            }


          }
    









      //////////////////////////////////////////////////////////////////////
       //////////////////////////////////////////////////////////////////////
       //// end scene collider
       //////////////////////////////////////////////////////////////////////

      if(hit.collider.CompareTag("floor_rachel"))

          {
 
/////////////////////////////////////////////////////////////////
///////// click on the floor and end it ////////////////////////
////////////////////////////////////////////////////////////////

       ///// if (aiming==false && done==false) 
        if (smashed == true) 
      
            {


               ////////////////////////////////
              /////////// advance scene
              /////////// with scene controller
              /////////////////////////////////
              SceneController.EndScene();
              Debug.Log("and, scene");

              /////////////////////////////////
            }

              ///////////////////////////////////////////////////////////////
              //// after the piggy has been picked up hit state and booleans
              ///////////////////////////////////////////////////////////////


          }
    



       
         if (readyToBreak==true && smashed == false) 
      
            {

        //////////////////////////////////////////////////////////////////////
        ///////// move the piggy to the playerCamera ////////////////////////
        //////////////////////////////////////////////////////////////////////


                    DiscardPiggy();//picks up piggy and triggers all the correspoinding triggers and booleans

            }

        





        }




      }






        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////// attach the piggy to player and set broken piggy and coin positions  ////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////

if (isHolding==true) 
          {



            piggybank.transform.position = (playerCamera.transform.forward*0.7f) + playerCamera.transform.position;
            piggybank.transform.localRotation = playerCamera.transform.rotation;
            readyToBreak=true;
          targetPosition.z =  (piggybank.transform.position.z*1.03f);
            targetPosition.x = piggybank.transform.position.x;
          targetPositionCoins.z =  (piggybank.transform.position.z*1.03f);
            targetPositionCoins.x = piggybank.transform.position.x;

 

}


        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////// set broken piggy and coins to target position on the floor and booleans   ///////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////

  if (thrown==true && smashed==false)

              {

      //////  Brokenpiggy.transform.rotation = piggybank.transform.rotation;
        Brokenpiggy.transform.position = targetPosition;
        coins_container.transform.position = targetPositionCoins;
       ///// coins_container.transform.rotation = Brokenpiggy.transform.rotation;

        Brokenpiggy.SetActive(true);
        coins_container.SetActive(true);


        coins.Play();
        breaking.Play();
        
        ////Debug.Log("piggy hit floor");

        smashed=true;


              }



    }


        //////////////////////////////////////////////////////////////////////
        ///////// booleans when you pick up piggy    ////////////////////////
        //////////////////////////////////////////////////////////////////////

    void PickUpPiggy()
    {  //pickup piggy,  the counter is reset to 0.
            isHolding=true;

          ///Debug.Log("piggy picked up");

         //Set gravity to false while holding it
         piggybank.GetComponent<Rigidbody>().useGravity = false;
        piggybank.GetComponent<Collider>().enabled = false;

    }


        //////////////////////////////////////////////////////////////////////
        ///////// booleans when yhou throw piggy     ////////////////////////
        //////////////////////////////////////////////////////////////////////

    void DiscardPiggy()
    {  //throw piggy

            piggybank.GetComponent<Rigidbody>().useGravity = true;
            piggybank.GetComponent<Collider>().enabled = true;

 
            StartCoroutine(ThrowPiggy());

     }



        //////////////////////////////////////////////////////////////////////
        ///////// actually throwing piggy routine     //////////////////////
        //////////////////////////////////////////////////////////////////////

 public IEnumerator ThrowPiggy(){

       ///// Debug.Log("piggy thrown");


      Transform t = piggybank.GetComponent<Transform>();
      Vector3 originalPosition = t.position;
 

       float counter = 0f;
      while (counter < 0.5f){
           t.position  = Vector3.Lerp (t.position, targetPosition, counter);
           counter += Time.deltaTime;
           ///yield return 0;
                    yield return null;

      }

 thrown=true;

 //// Destroy(piggybank);


            piggybank.SetActive(false);
 


yield break;
 }




        //////////////////////////////////////////////////////////////////////
        ///////// highlight color routine               //////////////////////
        //////////////////////////////////////////////////////////////////////


    IEnumerator FlashObject()
    {
        while (lookingAtObject == true)
        {
            yield return new WaitForSeconds(0.05f);
            if (flashingIn == true)
            {
                if (blueCol <= 30)
                {
                    flashingIn = false;
                }

                else
                {
                    blueCol -= 25;
                    greenCol -= 25;
                }
            }
            if (flashingIn == false)
            {
                if (blueCol >= 250)
                {
                    flashingIn = true;
                }
                else
                {
                    blueCol += 25;
                    greenCol += 25;
                }
            }
        }

    }





    // Highlight function
    void Highlight()
    {

        Color32 teal = new Color32((byte)redCol, (byte)greenCol, (byte)blueCol, 255);
        Color32 red = new Color32((byte)redCol, (byte)greenCol, (byte)blueCol, 255);
        GameObject obj = hit.transform.gameObject;
        Renderer r = obj.GetComponent<Renderer>();
        if (obj==alarmclock){

        r.material.color = red;

        /// Debug.Log(lookingAtObject);

        }
        r.material.color = teal;
        lookingAtObject = true;



        if (startedFlashing == false)
        {
            startedFlashing = true;
            StartCoroutine(FlashObject());
        }

    }
    // Unhighlight function
    void Unhighlight()
    {
        startedFlashing = false;
        lookingAtObject = false;
        StopCoroutine(FlashObject());
        Color32 white = new Color32(255, 255, 255, 255);
        GameObject obj = hit.transform.gameObject;
         

        Renderer w = obj.GetComponent<Renderer>();


        w.material.color = white;


    }


 




 

}

