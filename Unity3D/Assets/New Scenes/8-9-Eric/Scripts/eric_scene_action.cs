﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eric_scene_action : MonoBehaviour

{

    private Camera playerCamera;
    private GameObject DirectionalLight;
    private GameObject Capsule;
    private GameObject eric_walls;
    private GameObject Phone;
    private GameObject Paper;
    private GameObject ParticleSystem;
    private GameObject ReachableFloor;
    private GameObject KidsVoice;
    private GameObject capsuleObject = null;
    private GameObject BillParticles;

    ////public Texture billHighlight;
    private Material billHighlight;
    private Material billReg;

    Vector3 targetPosition;

    private float movementTime = 1f;
    private bool done = false;
    private bool aiming = false;
    private bool paperhit = false;
    private bool phonehit = false;

    private int stage = 1;
    private int counter = 0;

    private Light lt;


    // Start is called before the first frame update
    void Start()
    {
 
          billHighlight = Resources.Load("bulletinboard_bill_hover", typeof(Material)) as Material;
          billReg = Resources.Load("bulletinboard_bill", typeof(Material)) as Material;

         ///playerCamera = GameObject.Find("playerCamera");
          /////////////  playerCamera = GameObject.Find("MainCamera");
         //////// playerCamera = Camera.main.gameObject;
        playerCamera = Camera.main;

          
          DirectionalLight = GameObject.Find("DirectionalLight");
          Paper = GameObject.Find("Paper");
          eric_walls = GameObject.Find("eric_walls");
          Phone = GameObject.Find("Phone");
          ParticleSystem = GameObject.Find("ParticleSystem");
          ReachableFloor = GameObject.Find("ReachableFloor");
          KidsVoice = GameObject.Find("KidsVoice");
          BillParticles = GameObject.Find("BillParticles");


         //// billHighlight = Texture.Find("billHighlight");

          Phone.SetActive(false);
          eric_walls.SetActive(true);
          DirectionalLight.SetActive(true);
          ParticleSystem.SetActive(false);
          ReachableFloor.SetActive(true);
          KidsVoice.SetActive(false);
          BillParticles.SetActive(false);

  

    }
 


 
 void TriggerFunction() {
     StartCoroutine("FadeIn");//Start a coroutine to run independent from the update function
 }



    // Update is called once per frame
    void Update()
    {




    



//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
/////// set raycaster to the player camera
//////////////////////////////////////////////////////////////////////

      RaycastHit hit;
      Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

      /////////////////////////////////////////////////////////////////////    
      /////////////////////////////////////////////////////////////////////
      ////// set raycaster to the player camera
      //////////////////////////////////////////////////////////////////////

      targetPosition.x = playerCamera.transform.position.x;
      targetPosition.y = playerCamera.transform.position.y -.2f;
      ////  targetPosition.z = playerCamera.transform.position.z - .2f;
      targetPosition.z = playerCamera.transform.position.z - .2f;


            /////////////////////////////////////////////////////////////////////
           ////// Highlight paper by looking at it
           //////////////////////////////////////////////////////////////////////

           if ( Physics.Raycast (ray,out hit)) 
           {
              if(hit.collider.CompareTag("Paper"))

             {
     

              Paper.transform.localScale = new Vector3(0.375f, 0.525f, 0.60506f);
              /// Debug.Log("You hit the " + hit.transform.name); 
              //// Paper.GetComponent().material = moon;

              Paper.GetComponent<Renderer>().material = billHighlight;


                   ////// Paper.material.mainTexture = texture;

             } 
                else 

                {

                  Paper.GetComponent<Renderer>().material = billReg;

                  Paper.transform.localScale = new Vector3(0.3498515f, 0.5008999f, 0.60506f);
                }
          }

          /////////////
          ///////
          ///////
          //////////

             counter++;

          /////////////
          ///////
          ///////
          //////////

 
             if (counter >= 6000 && stage ==1 )
                  {
 

                      RemindAboutPaper();//highlights paper and triggers all the correspoinding triggers and booleans

                  }



 
             if (counter >= 6000 && stage ==2 )
                  {
 

                      PickUp();//picks up paper and triggers all the correspoinding triggers and booleans

                  }





 
             if (counter >= 600 && stage ==3 )
                  {
 

                      KidSaysMom();//kid says mom, mom, mom and triggers all the correspoinding triggers and booleans

                  }



 
             if (counter >= 6000 && stage ==4 )
                  {
 

                      DiscardPaper();//discards paper and triggers all the correspoinding triggers and booleans

                  }



 
             if (counter >= 6000 && stage ==5 )
                  {
 

                      DiscardPhone();//discards phone and triggers all the correspoinding triggers and booleans

                  }


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
/////// Interacting with objects on Button Action
//////////////////////////////////////////////////////////////////////

 if (SceneController.ActionButton)       

      {

        //////////
        ////////
        //////////////////////////////////////////////////////////////////////
        //// raycaster hit
        //////////////////////////////////////////////////////////////////////

        if ( Physics.Raycast (ray,out hit)) 
        {

        ///check intersection
       //////Debug.Log("You selected the " + hit.transform.name); 

       //////////////////////////////////////////////////////////////////////
       //////////////////////////////////////////////////////////////////////
       //// paper collider
       //////////////////////////////////////////////////////////////////////

      if(hit.collider.CompareTag("Paper"))

          {
 
/////////////////////////////////////////////////////////////////
///////// pulling the paper off the wall ////////////////////////
////////////////////////////////////////////////////////////////

        if (aiming==false && done==false) 
      
            {

        //////////////////////////////////////////////////////////////////////
        ///////// move the paper to the playerCamera ////////////////////////
        ///////// the paper and phone is positioned  at the bottom //////////            
        //////////////////////////////////////////////////////////////////////


                      PickUp();//picks up paper and triggers all the correspoinding triggers and booleans

            }

              ///////////////////////////////////////////////////////////////
              //// after the paper has been picked up hit state and booleans
              ///////////////////////////////////////////////////////////////


          if (paperhit==true)

              {

      

                      DiscardPaper();//gets rid of paper and swaps for phone and triggers all the correspoinding triggers and booleans
              }

          }
    

               //////////
               ////////
              ///////////////////////////////////////////////////////////////
               //// The phone collider activators
              ///////////////////////////////////////////////////////////////



          if(hit.collider.CompareTag("Phone"))
            {


                      DiscardPhone();//discards phone and triggers all the correspoinding triggers and booleans

             } 
          }

        }
 

 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
  /// updating the position of the paper and phone with the playerCamera
 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////

if (done==true) 
          {

            Paper.transform.position = (playerCamera.transform.forward*0.45f) + playerCamera.transform.position;
            Paper.transform.localScale = new Vector3(0.44f, 0.6f, 0.60506f);
            Paper.transform.localRotation = Quaternion.Euler((playerCamera.transform.eulerAngles.x+15.0f), playerCamera.transform.eulerAngles.y, playerCamera.transform.eulerAngles.z);
           
           //////////////////////////////////////////////////////////////////////////////////
           /////// old positioning without rotation /////////////////////////////////////////
           ////////////  Paper.transform.localRotation = playerCamera.transform.rotation; 
           //////////////////////////////////////////////////////////////////////////////////

            paperhit = true;


           }

if (phonehit==true) 
          {

            Phone.transform.position = (playerCamera.transform.forward*0.4f) + playerCamera.transform.position;
            Phone.transform.localRotation = playerCamera.transform.rotation;

           }
        
        }
   

    void RemindAboutPaper()
    {  //pickup paper,  the counter is reset to 0.

            
             ///////////////////////////////////////////////////////////////
            ///// remind about paper boolean controls
             ///////////////////////////////////////////////////////////////

                      Paper.transform.localScale = new Vector3(0.375f, 0.525f, 0.60506f);
                      Paper.GetComponent<Renderer>().material = billHighlight;
                      ///Debug.Log("you gotta pay the bill " + hit.transform.name); 
                        Debug.Log("Reminder: you gotta pay the bill "); 


                      /////////
                      ////////
                      //////
                        stage = 2;
                        counter = 0;
    }



    void PickUp()
    {  //pickup paper,  the counter is reset to 0.

            
             ///////////////////////////////////////////////////////////////
            ///// picked up paper boolean controls
             ///////////////////////////////////////////////////////////////

                      targetPosition *= -1.0f;
                      aiming = true;
                      done = true;
                      BillParticles.SetActive(true);


                      /////////
                      ////////
                      //////
                        stage = 3;
                        counter = 0;
    }



    void KidSaysMom()
    {  //Kid yells mom, mom ,  mom,  the counter is reset to 0.

            
             ///////////////////////////////////////////////////////////////
            ///// remind about paper boolean controls
             ///////////////////////////////////////////////////////////////

                      KidsVoice.SetActive(true);

                       /// Debug.Log("You hit the " + hit.transform.name); 


                      /////////
                      ////////
                      //////
                        stage = 4;
                        counter = 0;
    }





    void DiscardPaper()
    {  //get rid of the paper,  the counter is reset to 0.

            
             ///////////////////////////////////////////////////////////////
            ///// discard paper boolean controls
             ///////////////////////////////////////////////////////////////

/*
            ReachableFloor.SetActive(false);
            ParticleSystem.SetActive(true);
            DirectionalLight.SetActive(false);
            Phone.SetActive(true);
            eric_walls.SetActive(false);
            Paper.SetActive(false);
            KidsVoice.SetActive(false);
            BillParticles.SetActive(false);
*/
            phonehit=true;

                      /////////
                      ////////
                      //////
                        stage = 5;
                        counter = 0;

                           ////////////////////////////////
              /////////// advance scene
              /////////// with scene controller
              /////////////////////////////////
              SceneController.EndScene();
              /////////////////////////////////
      Debug.Log("End Scene"); 


    }








    void DiscardPhone()
    {  //Set the trigger at true so the gameobject can move to the right,the timer is at false and then the counter is reseted at 0.

            
             ///////////////////////////////////////////////////////////////
            ///// discard phone boolean controls
             ///////////////////////////////////////////////////////////////


              DirectionalLight.SetActive(true);
              Phone.SetActive(false);
              ParticleSystem.SetActive(false);
              ////// ReachableFloor.SetActive(true);

               ////////////////////////////////
              /////////// advance scene
              /////////// with scene controller
              /////////////////////////////////
              SceneController.EndScene();
              /////////////////////////////////


                      /////////
                      ////////
                      //////
                        counter = 0;

    }









    }
