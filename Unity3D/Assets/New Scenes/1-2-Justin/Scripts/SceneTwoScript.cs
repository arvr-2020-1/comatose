﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTwoScript : MonoBehaviour

{

    private GameObject playerCamera;
    public SceneController controller;    
    public GameObject sceneTwoObjects;
    public GameObject[] stampedPapers;
    public GameObject heldStamper;
    public GameObject stamper;
    private bool stampGrabbed; //boolean to check if we are currently stamping a paper
    private bool stampFinalGrabbed; //boolean to check if the stamp has been grabbed once
    public Material stampedMaterial;
    private int papersStamped;

    public AudioSource officeNoise;
    public AudioSource stampSound;



    // Start is called before the first frame update
    void Start()
    {
        stampGrabbed = false;
        stampFinalGrabbed = false;
        papersStamped = 0;
        playerCamera = Camera.main.gameObject;
        officeNoise.Play();
    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit hit;

        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);


        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Stamper") && !stampFinalGrabbed) //if we hit the stamper and have not grabbed the stamper once
            {
                if (SceneController.ActionButton) 
                { 
                    hit.collider.gameObject.GetComponent<RenderEnableDisable>().derenderObjects();
                    heldStamper.GetComponent<RenderEnableDisable>().renderObjects();
                    stampFinalGrabbed = true;
                    stampGrabbed = true;
                }
            }
            else if (hit.collider.CompareTag("Paper") && stampGrabbed && papersStamped != 6) //if we hit the paper with the raycast and have the stamp grabbed
            {
                GameObject paperObject = hit.collider.gameObject;

                if (SceneController.ActionButton)
                {

                    StartCoroutine(stampPaper(1.0f, paperObject)); //start the coroutine
                    if (papersStamped == 5)
                    {
                        StartCoroutine(EndScene(2.0f));
                    }
                    stampSound.Play();
                }
            }
        }
        Vector3 offset = new Vector3(0.1f, 0.5f, 0.6f);
        heldStamper.transform.position = Vector3.Lerp(heldStamper.transform.position, playerCamera.transform.position + playerCamera.transform.forward, 2f);
    }

    //ends the scene after duration and stopps the office noise
    IEnumerator EndScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        officeNoise.Stop();
        SceneController.EndScene();
    }

    IEnumerator stampPaper(float duration, GameObject paper)
    {
        //ddrender the held stamper
        stampGrabbed = false;
        heldStamper.GetComponent<RenderEnableDisable>().derenderObjects();

        //unhighlight the paper
        //        paper.GetComponent<HighlightObject>().materialChange(false);

        //get the position and rotation of the paper

        //change the angel and position of the stamp

        //render the stamp on the paper
        stamper.GetComponent<RenderEnableDisable>().renderObjects();

        //pause the coroutine
        yield return new WaitForSeconds(duration);

        //derender the stamp on the paper
        stamper.GetComponent<RenderEnableDisable>().derenderObjects();

        stampedPapers[papersStamped].GetComponent<RenderEnableDisable>().renderObjects();

        //add to the amt of papers stamped
        papersStamped++;

        //rerender the stampped object
        stampGrabbed = true;
        heldStamper.GetComponent<RenderEnableDisable>().renderObjects();
    }
    /*
    IEnumerator moveClock(GameObject clock)
    {
        float duration = 3.0f; //Takes 5 seconds for light to turn on

        while (duration >= 0.0f)
        {
            clock.transform.Translate(Vector2.up * Time.deltaTime);
            clock.transform.Translate(Vector3.forward * Time.deltaTime);
            clock.transform.Rotate(Vector2.up, 10f);
        }

        return null;
    }
    */
}
