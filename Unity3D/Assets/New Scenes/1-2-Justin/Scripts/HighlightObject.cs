﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightObject : MonoBehaviour
{
    private Material thisMaterial;
    public float glowIntensity;
    public float outlineThickness;

    // Start is called before the first frame update
    void Start()
    {
        thisMaterial = GetComponent<Renderer>().material;
        thisMaterial.SetFloat("_Intensity", 0f);
        thisMaterial.SetFloat("_Outline", 0f);
    }

    public void highlight()
    {
        thisMaterial.SetFloat("_Intensity", glowIntensity);
        thisMaterial.SetFloat("_Outline", outlineThickness);
    }

    public void highlight(float multiplier)
    {
        thisMaterial.SetFloat("_Intensity", glowIntensity * multiplier);
        thisMaterial.SetFloat("_Outline", outlineThickness * multiplier);
    }

    public void dehighlight()
    {
        thisMaterial.SetFloat("_Intensity", 0f);
        thisMaterial.SetFloat("_Outline", 0f);
    }
}
