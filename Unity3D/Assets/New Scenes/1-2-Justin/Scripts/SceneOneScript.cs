﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneOneScript : MonoBehaviour
{
    private GameObject playerCamera;
    private bool highlightedObject;
    public Light l;
    public GameObject[] clockParts;
    public SceneController controller;
    public GameObject sceneOneObjects;
 
    public AudioSource alarmOn;
    public AudioSource alarmOff;

    private AudioSource[] alarm;     

    // Start is called before the first frame update
    void Start()
    {
        highlightedObject = false;
        playerCamera = Camera.main.gameObject;
        alarm = GetComponents<AudioSource>();
        alarmOn = alarm[0];
        alarmOff = alarm[1];
        alarmOn.Play();

        playerCamera = Camera.main.gameObject;      
    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit hit;

        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Alarm")) //if we hit the alarm clock
            {
             
                highlightClock(l.intensity / 2.4f);
                highlightedObject = true;
                if (SceneController.ActionButton) //turn off the alarm clock
                {
                    alarmOn.Stop();
                    alarmOff.Play();                  
                    //hit.collider.GetComponent<AudioSource>().volume = 0;
                    StartCoroutine(EndScene(4.0f));
                }
            }

            else if (highlightedObject)
            {
                dehighlightClock();
                highlightedObject = false;
            }
        }
        else if (highlightedObject)
        {
            dehighlightClock();
            highlightedObject = false;
        }
    }

    //ends the scene after duration and stopps the office noise
    IEnumerator EndScene(float duration)
    {
        yield return new WaitForSeconds(duration);
        SceneController.EndScene();
    }

    private void dehighlightClock()
    {
        foreach (GameObject child in clockParts)
        {
            child.GetComponent<HighlightObject>().dehighlight();
        }
    }

    private void highlightClock()
    {
        foreach (GameObject child in clockParts)
        {
            child.GetComponent<HighlightObject>().highlight();
        }
    }

    private void highlightClock(float multiplier)
    {
        foreach (GameObject child in clockParts)
        {
            child.GetComponent<HighlightObject>().highlight(multiplier / 2);
        }
    }
}
