﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DimLight : MonoBehaviour
{
    public Light lt;

    void Start()
    {
        Invoke("TurnLightOn", 3.0f);//Invoke certain function after 3.0f seconds
    }

    void TurnLightOn()
    {
        StartCoroutine("FadeIn");
    }

    IEnumerator FadeIn()
    {
        float duration = 6.0f; //Takes 5 seconds for light to turn on
        float interval = 0.05f; //Updates per second
        lt.intensity = 0.0f;

        while (duration >= 0.0f)
        {
            lt.intensity += 0.02f;

            duration -= interval;
            yield return new WaitForSeconds(interval);//the coroutine will wait for 0.1 secs
        }
    }
}
