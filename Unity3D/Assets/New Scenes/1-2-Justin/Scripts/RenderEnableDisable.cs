﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderEnableDisable : MonoBehaviour
{

    public bool start;

    // Start is called before the first frame update
    void Start()
    {
        if (!start)
        {
            this.derenderObjects();
        }
    }
    
    /**
     * This function gets each of the children in a given object, turns on their collider and renderer.
     */

    public void renderObjects()
    {
        foreach (Transform child in transform)
        {
            Transform[] objects = child.GetComponentsInChildren<Transform>();
            if (child.GetComponent<SphereCollider>() != null)
            {
                child.GetComponent<SphereCollider>().enabled = true;
            }
            if (child.GetComponent<BoxCollider>() != null)
            {
                child.GetComponent<BoxCollider>().enabled = true;
            }
            foreach (Transform model in objects)
            {
                if (model.GetComponent<Renderer>() != null)
                {
                    model.GetComponent<Renderer>().enabled = true;
                }
            }
        }
    }

    /**
     * This function gets each of the children in a given object, turns off their collider and renderer.
     */

    public void derenderObjects()
    {
        foreach (Transform child in transform)
        {
            Transform[] objects = child.GetComponentsInChildren<Transform>();
            if (child.GetComponent<SphereCollider>() != null)
            {
                child.GetComponent<SphereCollider>().enabled = false;
            }
            if (child.GetComponent<BoxCollider>() != null)
            {
                child.GetComponent<BoxCollider>().enabled = false;
            }
            foreach (Transform model in objects)
            {
                if (model.GetComponent<Renderer>() != null)
                {
                    model.GetComponent<Renderer>().enabled = false;
                }
            }
        }
    }
}
