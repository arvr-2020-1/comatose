﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
  Vector3 objectPos;
  public bool canHold = true;
  public GameObject item;
  public GameObject tempParent;
  public bool isHolding = false;
  private GameObject playerCamera;

   void Start()
  {
        playerCamera = Camera.main.gameObject;
  }

  /* private void OnEnable()
    {
        SceneController.actionCallback += MouseDown;
        SceneController.actionCallback -= MouseDown;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= MouseDown;
    }
    
    */
    void Update()
    {

     RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if(hit.collider.CompareTag("Shoes") && SceneController.ActionButton)
            {
                if(isHolding == true)

                {
                item.GetComponent<Rigidbody>().velocity = Vector3.zero;
                item.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                item.transform.SetParent(tempParent.transform);
	            }

                else
                 {
                objectPos = item.transform.position; 
                item.transform.SetParent(null);
                 item.GetComponent<Rigidbody>().useGravity = true;
                item.transform.position = objectPos;
		         }
			}

          

         
    }

    void MouseDown()
    {
        //Physics.Raycast;

    isHolding = true;
    item.GetComponent<Rigidbody>().useGravity = false;
     item.GetComponent<Rigidbody>().detectCollisions = true;
	}

}
}
