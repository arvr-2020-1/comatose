﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoord;
    public GameObject shoesInTrash;
    public Collision shoesCollider;
    bool shoes;

    void OnMouseDown()

    {
        mZCoord = Camera.main.WorldToScreenPoint(
            gameObject.transform.position).z;

        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();

        if (Input.GetMouseButtonDown(0))
        {
            shoes = !shoes;
        }
        if (shoes)
        {
            shoesCollider.transform.gameObject.SetActive(true);
        }
        else
        {
            shoesCollider.transform.gameObject.SetActive(false);
        }

    }

    private Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;

        mousePoint.z = mZCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
    }

}
