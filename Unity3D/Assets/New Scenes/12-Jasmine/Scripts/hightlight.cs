﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hightlight : MonoBehaviour
{
  public Color startColor;
  public Color mouseOverColor;
  bool mouseOver = false;
   private GameObject playerCamera;


    private void Start()
    {
        playerCamera = Camera.main.gameObject;
    }



    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        
       RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if(hit.collider.CompareTag("Trash"))
            {
            mouseOver = true;
            GetComponent<Renderer>().material.SetColor("_Color", mouseOverColor);
			}

            else
            {   mouseOver = false;
               GetComponent<Renderer>().material.SetColor("_Color", startColor);
            
			}

        }

}




 /* void OnMouseEnter()
  {
  mouseOver = true;
  GetComponent<Renderer>().material.SetColor("_Color", mouseOverColor);
  }
  void OnMouseExit()
  {
   mouseOver = false;
  GetComponent<Renderer>().material.SetColor("_Color", startColor);
  }*/
}
