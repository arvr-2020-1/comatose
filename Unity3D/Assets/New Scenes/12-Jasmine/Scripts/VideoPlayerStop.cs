﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerStop : MonoBehaviour
{
    public GameObject videoToPlay;
   
    // Start is called before the first frame update
    void Start()
    {
        videoToPlay.SetActive(true);
    }

    // Update is called once per frame
   void OnTriggerEnter (Collider player)
   {
        if(player.gameObject.tag == "Tv" && SceneController.ActionButton)
           {
               videoToPlay.SetActive(false);
               Destroy(videoToPlay);
               
           }
   }
}
