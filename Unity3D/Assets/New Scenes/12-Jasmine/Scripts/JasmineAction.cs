﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JasmineAction : MonoBehaviour
{

    private GameObject playerCamera;
   public Material Black; 
   public Material blue;



    private void Start()
    {
      
        playerCamera = Camera.main.gameObject;
    }

    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        
       RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);

        if (Physics.Raycast(ray, out hit))
        {

            

             if (hit.collider.CompareTag("Shoes2") && SceneController.ActionButton)
            {
                hit.collider.transform.SetPositionAndRotation(new Vector3 (0,1.5f,.5f), Quaternion.Euler(new Vector3(0,0,0)));

            }

            if (hit.collider.CompareTag("Tv") && SceneController.ActionButton)
            {
                hit.collider.GetComponent<Renderer>().material = Black;

            }
           
        }

        //check when user committed actions 
  
    }
}
