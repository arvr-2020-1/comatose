﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateSound : MonoBehaviour
{
    public AudioClip sound;
    bool play;

    public AudioClip tvOff;
    
    //bool activate;
    /*  private void OnEnable()
    {
        SceneController.actionCallback += ActionButton;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButton;
    }
    */
    void Start()
    {
        play = true;
         GetComponent<AudioSource>().Play();
    }

    void OnMouseDown()
    {
        if (SceneController.ActionButton)
        {
            play = !play;
            
        }

        if (play)
        {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().PlayOneShot(tvOff);
        }
    
       
    }    
}
