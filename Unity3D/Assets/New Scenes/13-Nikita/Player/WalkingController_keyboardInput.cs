﻿/* 
 * author : jiankaiwang
 * description : The script provides you with basic operations of first personal control.
 * platform : Unity
 * date : 2017/12
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingController_keyboardInput : MonoBehaviour
{
    static Animator walkingAnimatorController;

    public float speed = 10f;
    private float translation;
    private float straffe;
    public GameObject headsetHead;
    public GameObject headsetHand;
    private GameObject[] shoes;
    

    void Start ()
    {
        walkingAnimatorController = GetComponent<Animator>();

        //turn off the cursor
        Cursor.lockState = CursorLockMode.Locked;

        //GameObject.FindWithTag("shoes").SetActive (false);
        //GameObject.FindWithTag("headsetHand").SetActive(false);
        //GameObject.FindWithTag("headsetHead").SetActive(false);
        shoes = GameObject.FindGameObjectsWithTag("shoes");         shoes[0].SetActive(false);
        shoes[1].SetActive(false);
        headsetHand.SetActive(false);
        headsetHead.SetActive(false);

    }

    public void EnableHeadset()
    {
        headsetHand.SetActive(true);
        headsetHead.SetActive(true);
        shoes[0].SetActive(true);
        shoes[1].SetActive(true);
    }

void Update () {
        // Input.GetAxis() is used to get the user's input
        // You can further set it on Unity. (Edit, Project Settings, Input)

        translation = Input.GetAxis("Vertical") * speed * .6f * Time.deltaTime;
        straffe = Input.GetAxis("Horizontal") * speed * .45f * Time.deltaTime;
        transform.Translate(straffe, 0, translation);

        if (Input.GetKey(KeyCode.DownArrow))
        {
            walkingAnimatorController.SetBool("isBackward", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isBackward", false);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            walkingAnimatorController.SetBool("isForward", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isForward", false);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            walkingAnimatorController.SetBool("isLeft", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isLeft", false);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            walkingAnimatorController.SetBool("isRight", true);
        }
        else
        {
            walkingAnimatorController.SetBool("isRight", false);
        }

        if (Input.GetMouseButton(0))
        {
            walkingAnimatorController.SetBool("headsetOff", true);
            headsetHead.SetActive(false);
        }
        else
        {
            walkingAnimatorController.SetBool("headsetOff", false);
        }

        if (Input.GetKeyDown("escape"))
        {
            // turn on the cursor
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
