﻿/* 
 * author : jiankaiwang
 * description : The script provides you with basic operations of first personal control.
 * platform : Unity
 * date : 2017/12
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingController : MonoBehaviour
{
    //static Animator walkingAnimatorController;

    public float speed = 10f;
    private float translation;
    private float straffe;
    //public GameObject headsetHead;
    //public GameObject headsetHand;

    void Start ()
    {
        //walkingAnimatorController = GetComponent<Animator>();

        //turn off the cursor
        Cursor.lockState = CursorLockMode.Locked;
    }
	
	void Update () {
        // Input.GetAxis() is used to get the user's input
        // You can further set it on Unity. (Edit, Project Settings, Input)

        translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        straffe = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        transform.Translate(straffe, 0, translation);

        //if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    walkingAnimatorController.SetBool("isBackward", true);
        //}
        //else
        //{
        //    walkingAnimatorController.SetBool("isBackward", false);
        //}

        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    walkingAnimatorController.SetBool("isForward", true);
        //}
        //else
        //{
        //    walkingAnimatorController.SetBool("isForward", false);
        //}

        //if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    walkingAnimatorController.SetBool("isLeft", true);
        //}
        //else
        //{
        //    walkingAnimatorController.SetBool("isLeft", false);
        //}

        //if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    walkingAnimatorController.SetBool("isRight", true);
        //}
        //else
        //{
        //    walkingAnimatorController.SetBool("isRight", false);
        //}

        //if (Input.GetMouseButton(0))
        //{
        //    walkingAnimatorController.SetBool("headsetOff", true);
        //    Destroy(headsetHead, 1);
        //    headsetHand.SetActive(true);
        //}
        //else
        //{
        //    walkingAnimatorController.SetBool("headsetOff", false);
        //    //headsetHand.SetActive(false);
        //}

        //if (Input.GetKeyDown("escape"))
        //{
        //    // turn on the cursor
        //    Cursor.lockState = CursorLockMode.None;
        //}
    }
}
