﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxScript : MonoBehaviour
{
    //Game Object references
    public GameObject playerCamera;
    public GameObject lid;
    public bool carrying;
    float distance = 0.7f;

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;
        carrying = false;
    }
    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
        carrying = true;
        GetComponent<Rigidbody>().isKinematic = false;
    }
    // check when user committed action
    private void ActionButtonPress()
    {
        if (lid.GetComponent<Action>().rayCollider == GetComponent<Collider>())
        {
            GetComponent<Collider>().enabled = true;
            carrying = true;
            Action.lookedAtLid = true;
            lid.GetComponent<BoxCollider>().enabled = false;
            Debug.Log("Hit");

        }
        else if (carrying == true)
        {

            carrying = false;
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Collider>().enabled = true;
            lid.GetComponent<Action>().isShaking = false;
            Debug.Log("out");

        }
    }




    void Update()
    {
        if (carrying)
        {
            transform.position = playerCamera.transform.forward * distance + playerCamera.transform.position;
            float angleX = playerCamera.transform.rotation.eulerAngles.x;
            lid.transform.rotation = Quaternion.AngleAxis(angleX, Vector3.right);
            GetComponent<Collider>().enabled = false;
        }
    }

}









