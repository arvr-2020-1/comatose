﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoeScript : MonoBehaviour
{
    //Game Object references
    public GameObject playerCamera;
    public GameObject Shoes;
    public GameObject audioshoes;
    public bool carrying;
    public float duration = 3.0f;
    float distance = 0.7f;
    private bool shoesGrabbed;
    private float shoegrabCount;

    void Start()
    {
        shoesGrabbed = false;
        shoegrabCount = 0;

    }

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;
        carrying = false;
    }
    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
        carrying = true;
        GetComponent<Rigidbody>().isKinematic = false;
    }
    // check when user committed action
    private void ActionButtonPress()
    {
        if (Shoes.GetComponent<Action>().rayCollider == GetComponent<Collider>() && carrying == false)
        {
            GetComponent<Collider>().enabled = true;
            carrying = true;
            shoegrabCount += 1;
            audioshoes.GetComponent<AudioSource>().Play(0);
            Debug.Log("Hit");

        }

        else if (carrying == true)
        {

            carrying = false;
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Collider>().enabled = true;
            Shoes.GetComponent<Action>().isShaking = false;
            shoesGrabbed = true;
            Debug.Log("Hit");
            StartCoroutine(endScene(5.0f));
            

        }
    }




    void Update()
    {
        if (carrying)
        {
            transform.position = playerCamera.transform.forward * distance + playerCamera.transform.position;
            transform.localRotation = playerCamera.transform.rotation;
            GetComponent<Collider>().enabled = false;
        }

    }

    IEnumerator endScene (float duration)
	{
        yield return new WaitForSeconds(duration);
        SceneController.EndScene();
        Debug.Log("end");

	}



}