﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour
{
    public float shakeSpeed;
    public float shakeIntensity;
    public Vector3 originPos;
    public bool isShaking = true;
    public static bool lookedAtLid = false;
    public Collider rayCollider;
    public Collider rayCollider1;


    public GameObject playerCamera;
    void Start()
    {

        originPos = transform.position;

    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit _hit = new RaycastHit();
        RaycastHit hit;
        // create ray that matches player  head direction
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward);

        //raycast the ray

        if (Physics.Raycast(ray, out hit))

        {

            //check that it intersects correct object
            rayCollider = hit.collider;
            if (hit.collider.CompareTag("Lid") || hit.collider.CompareTag("Shoes"))
            {

                float step = shakeSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, originPos + Random.insideUnitSphere * .01f, step);

                if (!lookedAtLid && !hit.collider.gameObject.GetComponent<AudioSource>().isPlaying)
                {
                    hit.collider.gameObject.GetComponent<AudioSource>().Play(0);

                }



            }




        }

    }
}






