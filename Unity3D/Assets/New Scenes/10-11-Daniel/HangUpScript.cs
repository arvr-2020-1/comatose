﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HangUpScript : MonoBehaviour
{
    public GameObject phone;
    public GameObject voiceOver;
    public GameObject raycast;
    public GameObject hangUp;
    public GameObject busyLine;

    private void OnEnable()
    {
        SceneController.actionCallback += ActionButtonPress;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionButtonPress;
    }

    private void ActionButtonPress()
    {
        if (raycast.GetComponent<RayDanielScene>().raycollider == GetComponent<Collider>())
        {
            phone.GetComponent<PhoneScript>().carrying = false;
            phone.GetComponent<Collider>().enabled = true;
            phone.transform.position = new Vector3(.581f, 1.055f, -1.225f);
            phone.transform.rotation = Quaternion.Euler(274.5f, 258.403f, 91.743f);
            hangUp.GetComponent<AudioSource>().Play(0);
            voiceOver.GetComponent<AudioSource>().Pause();
            busyLine.GetComponent<AudioSource>().Stop();
            GetComponent<Collider>().enabled = false;
        }

    }
}
