﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayDanielScene : MonoBehaviour
{

    public GameObject reticleYellow;
    private GameObject cameraController;
    public bool hitting;
    public Collider raycollider;
    void Start()
    {
        reticleYellow.SetActive(false);
        cameraController = Camera.main.gameObject;
    }

        void Update()
        {
            RaycastHit hit;

            Ray ray = new Ray(cameraController.transform.position, cameraController.transform.forward);

            Debug.DrawRay(cameraController.transform.position, cameraController.transform.forward);

            raycollider = null;

            if (Physics.Raycast(ray, out hit))
            {
                raycollider = hit.collider;    

                if (hit.collider.CompareTag("Item"))
                {
                    reticleYellow.SetActive(true);
                    hitting = true;
                }

                else
                {
                    reticleYellow.SetActive(false);
                    hitting = false;
             
                }
            }
        }
}
