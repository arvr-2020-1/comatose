﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleKnife : MonoBehaviour
{
    public enum CutStep { NO_KNIFE, KNIFE_MOVING, KNIFE_READY, KNIFE_CUTTING };

    public GameObject knife;
    public float distance=0.5f;
    public float speed = 1.0f;

    LensFlare flare;
    CutStep cutStep;
    float delta;
    Vector3 target;

    public AudioSource tomatoCut;

    // Start is called before the first frame update
    void Start()
    {
        flare = GetComponent<LensFlare>();
        cutStep = CutStep.NO_KNIFE;
        flare.enabled = true;
        delta = 0.01f; //1cm
    }

    //Enables SceneController callbacks
    private void OnEnable()
    {
        SceneController.actionCallback += ActionEvent;
    }

    private void OnDisable()
    {
        SceneController.actionCallback -= ActionEvent;
    }

    //Called when the user presses the action button
    void ActionEvent()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
        {

            switch (cutStep)
            {
                case CutStep.NO_KNIFE:
                    if (hit.transform.CompareTag("Knife"))
                    {
                        cutStep = CutStep.KNIFE_MOVING;
                        knife.GetComponent<BoxCollider>().enabled = false;
                        flare.enabled = false;
                    }
                    break;

        
                case CutStep.KNIFE_READY:
                    if (hit.transform.CompareTag("Item")) //tomato
                    {
                        knife.transform.position = hit.transform.position;
                        hit.transform.parent.gameObject.GetComponent<Slice>().CutHalf();
                        cutStep = CutStep.KNIFE_CUTTING;

                        tomatoCut.Play();                     
                    }
                    break;


            }

        }

    }


    // Update is called once per frame
    void Update()
    {

        
        //updates the flare animation and the knife position when not cutting
        if (flare.enabled)
            flare.brightness = Mathf.Sin(Time.time);

        if(cutStep != CutStep.KNIFE_CUTTING)
            target = Camera.main.transform.position + Camera.main.transform.forward * distance;


        switch (cutStep)
        {
            case CutStep.KNIFE_MOVING:
                
                knife.transform.position = Vector3.MoveTowards(knife.transform.position, target, speed);
                if ((target - knife.transform.position).magnitude < delta)
                    cutStep = CutStep.KNIFE_READY;
                break;

            case CutStep.KNIFE_CUTTING: //skip a frame  
                    cutStep = CutStep.KNIFE_MOVING;
                break;

            case CutStep.KNIFE_READY:
                knife.transform.position = target;
                knife.transform.rotation =  Quaternion.RotateTowards(knife.transform.rotation,Camera.main.transform.rotation*Quaternion.Euler(0.0f,0.0f,90),speed*10f);
                break;

        }

       

        
    }
}
