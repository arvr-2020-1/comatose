﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slice : MonoBehaviour
{
    public GameObject original;
    public GameObject sliced;
   

    // Start is called before the first frame update
    void Start()
    {
        sliced.SetActive(false);
       
    }

    public void CutHalf()
    {
        original.SetActive(false);
        sliced.SetActive(true);
        sliced.transform.DetachChildren();

    }

    // Update is called once per frame
    void Update()
    {
   
        
    }
}
