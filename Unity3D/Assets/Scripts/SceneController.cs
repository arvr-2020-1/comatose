﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

//Move from one scene to the next
//Must exist in hierarchy
//Abstract input from controller and mouse/keyboard
public class SceneController : MonoBehaviour
{

    private int nextScene;
    private InputDevice inputDevice;
    private bool oldEndSceneButton = false;
    private bool oldActionButton = false;
    static private bool actionButtonPressed = false;
    //singleton
    static SceneController sceneController = null;

    //public interface
    static public bool ActionButton { get { return actionButtonPressed; } }
    public delegate void ActionButtonDownCallback();
    public static event ActionButtonDownCallback actionCallback;

    //editor interface
    [SerializeField] public float timeToUnload;
    [SerializeField] public string[] sceneNames;
    public GameObject intro; //objects for the intro scene

    public WalkingController_keyboardInput walkingController_temporary;


    public delegate void SceneTransitionCallback();
    public static event SceneTransitionCallback sceneTransitionCallback;

    private void OnEnable()
    {
        sceneController = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        nextScene = 0;
        timeToUnload = 1.0f;
        var leftHandedControllers = new List<UnityEngine.XR.InputDevice>();
        var desiredCharacteristics = UnityEngine.XR.InputDeviceCharacteristics.HeldInHand | UnityEngine.XR.InputDeviceCharacteristics.Controller;
        UnityEngine.XR.InputDevices.GetDevicesWithCharacteristics(desiredCharacteristics, leftHandedControllers);

        foreach (var device in leftHandedControllers)
        {
            inputDevice = device;
            Debug.Log(string.Format("Device name '{0}' has characteristics '{1}'", device.name, device.characteristics.ToString()));
        }

    }

    void OnMouseDown()
    {
        actionCallback?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {

        bool endSceneButton;

        inputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxisClick, out endSceneButton);   //advance scene debug 
        inputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out actionButtonPressed);          //click items in game

        if (Input.GetMouseButtonDown(0))
            actionButtonPressed = true;

        //transition to pressed
        if(!oldActionButton && actionButtonPressed)
        {
            actionCallback?.Invoke();
        }

        //advance scene manually
        if (Input.GetKeyDown("space") || (endSceneButton && !oldEndSceneButton))
        {
            AdvanceScene();
        }

        oldEndSceneButton = endSceneButton;
        oldActionButton = actionButtonPressed;

    }

    //Interface called when others want to advance the scene
    public static void EndScene()
    {
        sceneController.AdvanceScene();
    }


    //called when one of the scenes changed state
    public static void UnloadScene()
    {
            sceneController.Unload();
    }

    void AdvanceScene()
    {
        if (nextScene < sceneNames.Length)
        {
            SceneManager.LoadScene(sceneNames[nextScene], LoadSceneMode.Additive);
            Debug.Log("Loading" + sceneNames[nextScene]);

            //do not unload the first scene (just hide)
            if (nextScene == 0)
            {
                intro.SetActive(false);
            }

            //send signal for fadeout
            //check a way for only the old scene receive this
            sceneTransitionCallback?.Invoke();

            nextScene++;

            if (nextScene == sceneNames.Length) //if last scene, teleport
                walkingController_temporary.EnableHeadset();

        }
    }

    void Unload()
    {
        if (nextScene > 1)
        {
            Debug.Log("unloading" + sceneNames[nextScene - 2]);
            SceneManager.UnloadSceneAsync(sceneNames[nextScene - 2], UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }
    }


}
