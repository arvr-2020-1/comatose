﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : MonoBehaviour
{
    public float length;
    public float secondsPerUpdate;
    public Material[] materials;
    bool visible;

    //register a callback from when this scene finish loading
    private void OnEnable()
    {
        //subscribe so that we know once scene has loaded completely
        SceneManager.sceneLoaded += OnSceneLoaded;

        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_DissolveTime", 1.5f);
        }
        visible = false;
        length = 5f;
        secondsPerUpdate = 0.01f;
    }

    //scene has finished loading warn the sceneController
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        FadeIn();

        //subscribe event for when it is time to disappear
        SceneController.sceneTransitionCallback += OnUnload;
    }

    //called by scenecontroller when this scene should transiton
    void OnUnload()
    {
        if (visible)
            FadeOut();

        //unsubscribe since it is not needed anymore (scene is going to be unloaded)
        SceneController.sceneTransitionCallback -= OnUnload;
    }

    //scene is unloaded unregister the event
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;

    }


    public void FadeOut()
    {
        StartCoroutine(Animate(length,-1.5f,1.5f));
    }

    public void FadeIn()
    {
        StartCoroutine(Animate(length, 1.5f, -1.5f));
    }

    IEnumerator Animate(float duration,float start, float end)
    {
        float startTime = Time.time;

        while ((Time.time - startTime) < duration)
        {
            for (int j = 0; j < materials.Length; j++)
                materials[j].SetFloat("_DissolveTime", start + (end-start)/duration * (Time.time - startTime));
            yield return new WaitForSeconds(secondsPerUpdate);
        }
        visible = !visible;

        if(visible)
            SceneController.UnloadScene();
    }

}
